"""Test Jobard client."""

import pytest
from requests_mock.mocker import Mocker

from jobard_client.client import JobardClient
from jobard_client.core.jobs.models import JobOrderSubmit
from jobard_client.exceptions import JobardError, UnknownJobError
from tests.conftest import build_url


def test_submit(jobard_client: JobardClient, requests_mock: Mocker):
    requests_mock.post(build_url('job-orders'), status_code=201, text='1')
    joborder_id = jobard_client.submit(JobOrderSubmit(command=['echo', '-n', '"Hello World !\n"']), token='token')
    assert joborder_id is not None
    assert joborder_id == 1


def test_job_ok(jobard_client: JobardClient, requests_mock: Mocker):
    request_url = build_url('jobs', {'job_id': 1})
    requests_mock.get(request_url, status_code=201, json={'job_id': 1})
    job_id = jobard_client.job(job_id=1)
    assert job_id is not None
    assert job_id == 1


@pytest.mark.parametrize('status,expected', [
    (400, UnknownJobError),
    (500, JobardError),
])
def test_job_error(jobard_client: JobardClient, requests_mock: Mocker, status, expected):
    request_url = build_url('jobs', {'job_id': 1})
    requests_mock.get(request_url, status_code=status)
    with (pytest.raises(expected)):
        jobard_client.job(job_id=1)
