"""Test Jobard client application."""
import unittest

from click.testing import CliRunner

from jobard_client.cli.application import cli
from jobard_client.exceptions import CancellationError, JobardError

OUTPUT_DEBUG_MODE_OFF = 'Debug mode is off'
OUTPUT_DEFAULT_API = 'API : http://localhost:8000'

EXIT_CODE_OK = 0
EXIT_CODE_EXCEPTION = 1
EXIT_CODE_ERROR = 2

ID_NOT_INTEGER = 'fake'
ID_UNKNOWN = '9999999999999999999999999999999999999999999999999'
ID_CANCELLATION = '1'

COMMAND_STAT = 'stat'
COMMAND_CANCEL = 'cancel'
COMMAND_REPORT = 'report'
COMMAND_LIST = 'list'
COMMAND_SUBMIT = 'submit'

TEST_ID = '1'
INVALID_INTEGER_MESSAGE = 'is not a valid integer'


class SubmitTestCase(unittest.TestCase):
    """Submit command test case."""

    def test_submit_success(self):
        """Test submit command success."""
        runner = CliRunner()
        test_result = runner.invoke(
            cli,
            [
                COMMAND_SUBMIT,
                'my_command',
                '--priority',
                '1',
                '--cores',
                '4',
                '--memory',
                '16G',
                '--walltime',
                '00:30:00',
            ],
        )
        self.assertEqual(EXIT_CODE_OK, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)

    def test_submit_failure_invalid_memory(self):
        """Test submit command failure with invalid memory."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_SUBMIT, 'another_command', '--memory', '120'])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn("Invalid value for '--memory': 120 is not a valid memory", test_result.output)

    def test_submit_failure_invalid_walltime(self):
        """Test submit command failure with invalid walltime."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_SUBMIT, 'foo', '--walltime', '00:30'])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn("Invalid value for '--walltime': 00:30 is not a valid walltime", test_result.output)

    def test_submit_failure_invalid_split(self):
        """Test submit command failure with invalid split."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_SUBMIT, 'bar', '--split', 'wrong_value'])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn("Invalid value for '--split': wrong_value is not a valid split", test_result.output)


class ListTestCase(unittest.TestCase):
    """List command test case."""

    def test_list_success(self):
        """Test list command success."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_LIST, TEST_ID])
        self.assertEqual(EXIT_CODE_OK, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)

    def test_list_failure_unknown_state(self):
        """Test list command failure with invalid state."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_LIST, TEST_ID, '--state', 'NEW', '--state', 'EXOTIC_STATE'])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn("Invalid value for '--state': EXOTIC_STATE is not a valid state", test_result.output)

    def test_list_failure_not_integer_job_id(self):
        """Test list command failure with job_order_id not an integer."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_LIST, ID_NOT_INTEGER])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(INVALID_INTEGER_MESSAGE, test_result.output)

    def test_list_failure_unknown_job_order_id(self):
        """Test list command failure with unknown job_order_id."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_LIST, ID_UNKNOWN])
        self.assertEqual(EXIT_CODE_EXCEPTION, test_result.exit_code)
        self.assertIsInstance(test_result.exception, JobardError)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobOrderID : {0}'.format(ID_UNKNOWN), test_result.output)


class ReportTestCase(unittest.TestCase):
    """Report command test case."""

    def test_report_success(self):
        """Test report command success."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_REPORT, TEST_ID])
        self.assertEqual(EXIT_CODE_OK, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobID : 1', test_result.output)
        self.assertIn('\"id\": 1', test_result.output)

    def test_report_failure_not_integer_job_id(self):
        """Test report command failure with job_id not an integer."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_REPORT, ID_NOT_INTEGER])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(INVALID_INTEGER_MESSAGE, test_result.output)

    def test_report_failure_unknown_job_id(self):
        """Test report command failure with unknown job_id."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_REPORT, ID_UNKNOWN])
        self.assertEqual(EXIT_CODE_EXCEPTION, test_result.exit_code)
        self.assertIsInstance(test_result.exception, JobardError)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobID : {0}'.format(ID_UNKNOWN), test_result.output)


class StatTestCase(unittest.TestCase):
    """Stat command test case."""

    def test_stat_success(self):
        """Test stat command success."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_STAT, TEST_ID])
        self.assertEqual(EXIT_CODE_OK, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobOrderID : 1', test_result.output)
        self.assertIn('progress', test_result.output)

    def test_stat_failure_not_integer_job_order_id(self):
        """Test stat command failure with job_order_id not an integer."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_STAT, ID_NOT_INTEGER])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(INVALID_INTEGER_MESSAGE, test_result.output)

    def test_stat_failure_unknown_job_order_id(self):
        """Test stat command failure with unknown job_order_id."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_STAT, ID_UNKNOWN])
        self.assertEqual(EXIT_CODE_EXCEPTION, test_result.exit_code)
        self.assertIsInstance(test_result.exception, JobardError)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobOrderID : {0}'.format(ID_UNKNOWN), test_result.output)


class CancelTestCase(unittest.TestCase):
    """Cancel command test case."""

    def test_cancel_failure_not_cancellable_state(self):
        """Test cancel command failure on not cancellable state."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_CANCEL, ID_CANCELLATION])
        self.assertEqual(EXIT_CODE_EXCEPTION, test_result.exit_code)
        self.assertIsInstance(test_result.exception, CancellationError)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)

    # TODO faire dépendre d'un submit avec état RUNNING et récupérer l'id ?
    def test_cancel_success(self):
        """Test cancel command success."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_CANCEL, ID_CANCELLATION])
        self.assertEqual(0, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn(
            'Cancellation for JobOrder {0} successfully registered'.format(ID_CANCELLATION),
            test_result.output,
        )

    def test_cancel_failure_not_integer_job_order_id(self):
        """Test cancel command failure with job_order_id not an integer."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_CANCEL, ID_NOT_INTEGER])
        self.assertEqual(EXIT_CODE_ERROR, test_result.exit_code)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(INVALID_INTEGER_MESSAGE, test_result.output)

    def test_cancel_failure_unknown_job_order_id(self):
        """Test cancel command failure with unknown job_order_id."""
        runner = CliRunner()
        test_result = runner.invoke(cli, [COMMAND_CANCEL, ID_UNKNOWN])
        self.assertEqual(EXIT_CODE_EXCEPTION, test_result.exit_code)
        self.assertIsInstance(test_result.exception, JobardError)
        self.assertIn(OUTPUT_DEBUG_MODE_OFF, test_result.output)
        self.assertIn(OUTPUT_DEFAULT_API, test_result.output)
        self.assertIn('JobOrderID : {0}'.format(ID_UNKNOWN), test_result.output)


if __name__ == '__main__':
    unittest.main()
