"""Test Jobard client mock."""
import unittest

import pytest
from requests_mock import Mocker

from jobard_client.client import ENDPOINT_JOB_ORDERS, ENDPOINT_JOBS, JobardClient
from jobard_client.core.core_types import State
from jobard_client.exceptions import (
    CancellationUnauthorizedError,
    InvalidJobOrderError,
    JobardError,
    UnknownJobError,
    UnknownJobOrderError,
)
from tests.conftest import build_url

DETAIL = 'detail'
NOT_INTEGER_ID = 'not_an_integer_id'
INVALID_JOB_ORDER_ERROR = 'Invalid jobOrder.'
UNPROCESSABLE_ENTITY = 'Response status : 422'


def _build_submit_url():
    return '{0}'.format(_build_job_orders_url())


def _build_job_orders_url():
    return build_url(ENDPOINT_JOB_ORDERS)


class MockTestCase(unittest.TestCase):
    """Mock test case."""

    def setUp(self):
        """Set up."""
        self.jobard_client = JobardClient()


class SubmitTestCase(MockTestCase):
    """Submit test case."""

    def test_submit_success(self):
        """Test submit command success."""
        with Mocker() as mocker:
            mocker.post(
                _build_submit_url(),
                status_code=201,
                json={'joborder_id': 1},
            )
            response = self.jobard_client.submit(command='my_command')
            assert response is not None
            assert response == {'joborder_id': 1}

    def test_submit_failure_invalid_memory(self):
        """Test submit failure with invalid memory."""
        with Mocker() as mocker:
            mocker.post(
                _build_submit_url(),
                status_code=422,
                text='{"command": "other_command", "memory": "512"}',
            )
            with pytest.raises(InvalidJobOrderError):
                self.jobard_client.submit(command='memory_command', memory='512')
            try:
                self.jobard_client.submit(command='memory_command', memory='512')
            except InvalidJobOrderError as invalid_job_order_error:
                assert invalid_job_order_error.args[0] == INVALID_JOB_ORDER_ERROR

    def test_submit_failure_invalid_walltime(self):
        """Test submit failure with invalid walltime."""
        with Mocker() as mocker:
            mocker.post(
                _build_submit_url(),
                status_code=422,
                text='{"command": "other_command", "walltime": "00:05"}',
            )
            with pytest.raises(InvalidJobOrderError):
                self.jobard_client.submit(command='walltime_command', walltime='00:05')
            try:
                self.jobard_client.submit(command='walltime_command', walltime='00:05')
            except InvalidJobOrderError as invalid_job_order_error:
                assert invalid_job_order_error.args[0] == INVALID_JOB_ORDER_ERROR

    def test_submit_failure_invalid_split(self):
        """Test submit failure with invalid split."""
        with Mocker() as mocker:
            mocker.post(
                _build_submit_url(),
                status_code=422,
                text='{"command": "other_command", "split": "wrong_value"}',
            )
            with pytest.raises(InvalidJobOrderError):
                self.jobard_client.submit(command='split_command', split='wrong_value')
            try:
                self.jobard_client.submit(command='split_command', split='wrong_value')
            except InvalidJobOrderError as invalid_job_order_error:
                assert invalid_job_order_error.args[0] == INVALID_JOB_ORDER_ERROR


class ListTestCase(MockTestCase):
    """List test case."""

    def test_list_success(self):
        """Test list success."""
        expected_json_response = {
            'count': 12,
            'offset': 0,
            'limit': 1000,
            'jobs': [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 5],
        }
        with Mocker() as mocker:
            mocker.get(
                '{0}/1/jobs/?offset=0&limit=1000'.format(_build_job_orders_url()),
                status_code=200,
                json=expected_json_response,
            )
            response = self.jobard_client.jobs(job_order_id=1)
            assert response is not None
            assert response == expected_json_response

    def test_list_failure_unknown_state(self):
        """Test list failure with invalid state."""
        with Mocker() as mocker:
            mocker.get(
                '{0}/1/jobs/?offset=0&limit=1000&state=EXOTIC'.format(_build_job_orders_url()),
                status_code=422,
            )
            with pytest.raises(ValueError):
                self.jobard_client.jobs(job_order_id=1, state_list=[State('EXOTIC')])
            try:
                self.jobard_client.jobs(job_order_id=1, state_list=[State('EXOTIC')])
            except ValueError as value_error:
                assert value_error.args[0] == "'EXOTIC' is not a valid State"

    def test_list_failure_not_integer_job_id(self):
        """Test list failure with job_order_id not an integer."""
        with Mocker() as mocker:
            mocker.get(
                '{0}/{1}/jobs/?offset=0&limit=1000'.format(_build_job_orders_url(), NOT_INTEGER_ID),
                status_code=422,
            )
            with pytest.raises(JobardError):
                self.jobard_client.jobs(NOT_INTEGER_ID)
            try:
                self.jobard_client.jobs(NOT_INTEGER_ID)
            except JobardError as jobard_api_error:
                assert jobard_api_error.args[0] == UNPROCESSABLE_ENTITY

    def test_list_failure_unknown_job_order_id(self):
        """Test list failure with unknown job_order_id."""
        with Mocker() as mocker:
            mocker.get(
                '{0}/2024/jobs/?offset=0&limit=1000'.format(_build_job_orders_url()),
                status_code=400,
                json={DETAIL: 'Unknown Job order ID : 2024'},
            )
            with pytest.raises(UnknownJobOrderError):
                self.jobard_client.jobs(2024)
            try:
                self.jobard_client.jobs(2024)
            except UnknownJobOrderError as unknown_job_order_error:
                assert unknown_job_order_error.args[0] == 'Unknown JobOrder ID 2024'


class ReportTestCase(MockTestCase):
    """Report test case."""

    def test_report_success(self):
        """Test report success."""
        expected_json_response = {
            'id': 1,
            'job_array': 1,
            'job_array_index': 1,
            'command_with_parameters': 'my_command',
            'scheduled_id': None,
            'state': 'NEW',
            'progress': None,
            'created': '2021-11-24T15:57:44.840145',
            'updated': '2021-11-24T15:57:44.840145',
            'submitted': None,
            'finished': None,
            'error_message': None,
        }
        with Mocker() as mocker:
            mocker.get(
                '{0}/1'.format(build_url(ENDPOINT_JOBS)),
                status_code=200,
                json=expected_json_response,
            )
            response = self.jobard_client.job(1)
            assert response is not None
            assert response == expected_json_response

    def test_report_failure_not_integer_job_id(self):
        """Test report failure with job_id not an integer."""
        with Mocker() as mocker:
            mocker.get('{0}/{1}'.format(build_url(ENDPOINT_JOBS), NOT_INTEGER_ID), status_code=422)
            with pytest.raises(JobardError):
                self.jobard_client.job(NOT_INTEGER_ID)
            try:
                self.jobard_client.job(NOT_INTEGER_ID)
            except JobardError as jobard_api_error:
                assert jobard_api_error.args[0] == UNPROCESSABLE_ENTITY

    def test_report_failure_unknown_job_id(self):
        """Test report failure with unknown job_id."""
        with Mocker() as mocker:
            mocker.get(
                '{0}/2023'.format(build_url(ENDPOINT_JOBS)),
                status_code=400,
                json={DETAIL: 'Unknown Job ID : 2023'},
            )
            with pytest.raises(UnknownJobError):
                self.jobard_client.job(2023)
            try:
                self.jobard_client.job(2023)
            except UnknownJobError as unknown_job_error:
                assert unknown_job_error.args[0] == 'Unknown Job ID 2023'


class StatTestCase(MockTestCase):
    """Stat test case."""

    def test_stat_success(self):
        """Test stat success."""
        expected_json_response = {
            'id': 1,
            'state': 'CANCELLATION_ASKED',
            'created': '2021-11-24T15:57:07.489702',
            'updated': '2021-12-02T14:40:30.508655',
            'submitted': None,
            'finished': None,
            'job_count': 0,
            'progress': {
                'percentage': 0,
                'job_states': {
                    'NEW': 6,
                    'ENQUEUED': 1,
                    'RUNNING': 2,
                    'SUCCEEDED': 1,
                    'FAILED': 1,
                    'CANCELLED': 1,
                },
            },
        }

        with Mocker() as mocker:
            mocker.get(
                '{0}/1/stat'.format(_build_job_orders_url()),
                status_code=200,
                json=expected_json_response,
            )
            response = self.jobard_client.stat(1)
            assert response is not None
            assert response == expected_json_response

    def test_stat_failure_not_integer_job_order_id(self):
        """Test stat failure with job_order_id not an integer."""
        with Mocker() as mocker:
            mocker.get('{0}/{1}/stat'.format(_build_job_orders_url(), NOT_INTEGER_ID), status_code=422)
            with pytest.raises(JobardError):
                self.jobard_client.stat(NOT_INTEGER_ID)
            try:
                self.jobard_client.stat(NOT_INTEGER_ID)
            except JobardError as jobard_api_error:
                assert jobard_api_error.args[0] == UNPROCESSABLE_ENTITY

    def test_stat_failure_unknown_job_order_id(self):
        """Test stat failure with unknown job_order_id."""
        with Mocker() as mocker:
            mocker.get(
                '{0}/2022/stat'.format(_build_job_orders_url()),
                status_code=400,
                json={DETAIL: 'Unknown Job order ID : 2022'},
            )
            with pytest.raises(UnknownJobOrderError):
                self.jobard_client.stat(2022)
            try:
                self.jobard_client.stat(2022)
            except UnknownJobOrderError as unknown_job_order_error:
                assert unknown_job_order_error.args[0] == 'Unknown JobOrder ID 2022'


class CancelTestCase(MockTestCase):
    """Cancel test case."""

    def test_cancel_ok(self):
        """Test cancel success."""
        with Mocker() as mocker:
            mocker.delete(
                '{0}/1'.format(_build_job_orders_url()),
                status_code=200,
                json={DETAIL: 'Cancellation for JobOrder 1 successfully registered'},
            )
            response = self.jobard_client.cancel(1)
            assert response is not None
            assert response[DETAIL] == 'Cancellation for JobOrder 1 successfully registered'

    def test_cancel_error_not_an_integer_id(self):
        """Test cancel failure with job_order_id not an integer."""
        with Mocker() as mocker:
            mocker.delete('{0}/{1}'.format(_build_job_orders_url(), NOT_INTEGER_ID), status_code=422)
            with pytest.raises(JobardError):
                self.jobard_client.cancel(NOT_INTEGER_ID)
            try:
                self.jobard_client.cancel(NOT_INTEGER_ID)
            except JobardError as jobard_api_error:
                assert jobard_api_error.args[0] == UNPROCESSABLE_ENTITY

    def test_cancel_failure_not_cancellable_state(self):
        """Test cancel failure on not cancellable state."""
        with Mocker() as mocker:
            err = 'Job order state RUNNING does not allow cancellation'
            mocker.delete(
                '{0}/12'.format(_build_job_orders_url()),
                status_code=400,
                json={DETAIL: err},
            )
            with pytest.raises(JobardError):
                self.jobard_client.cancel(12)
            try:
                self.jobard_client.cancel(12)
            except CancellationUnauthorizedError as cancellation_error:
                assert cancellation_error.args[0] == 'Cancellation error JobOrder ID 12 : {0}'.format(err)

    def test_cancel_failure_unknown_job_order_id(self):
        """Test cancel failure with unknown job_order_id."""
        with Mocker() as mocker:
            mocker.delete(
                '{0}/2021'.format(_build_job_orders_url()),
                status_code=400,
                json={DETAIL: 'Unknown Job order ID : 2021'},
            )
            with pytest.raises(JobardError):
                self.jobard_client.cancel(2021)
            try:
                self.jobard_client.cancel(2021)
            except CancellationUnauthorizedError as cancellation_error:
                assert cancellation_error.args[0] == 'Cancellation error JobOrder ID 2021 : Unknown Job order ID : 2021'


if __name__ == '__main__':
    unittest.main()
