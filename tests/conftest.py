"""Common utilities and fixtures."""
from typing import Dict, Optional
from urllib.parse import urlencode, urlunsplit

import pytest

from jobard_client.client import JobardClient

SERVER = 'localhost'
PORT = 80
JOBARD_API_URL = 'http://{0}:{1}'.format(SERVER, PORT)


def build_url(path: str, url_parameters: Optional[Dict] = None) -> str:
    if url_parameters is None:
        url_parameters = {}
    encoded_parameters = urlencode(url_parameters)
    domain = '{0}:{1}'.format(SERVER, PORT)
    return urlunsplit(('http', domain, path, encoded_parameters, ''))


@pytest.fixture(scope='session')
def jobard_client() -> JobardClient:
    return JobardClient(JOBARD_API_URL)
