"""HtCondor API Submit test."""
import pandas as pd

from jobard_client.client import JobardClient
from jobard_client.core.exceptions import JobardError
from jobard_client.core.jobs.models import JobOrderSubmit


def my_test():
    """HtCondor API Submit test."""

    client = JobardClient('http://localhost:8000')
    token = 'access_token'

    arguments = []

    df = pd.read_csv('/path/to/files.csv', header=None)
    df = df.reset_index()
    for index, row in df.iterrows():
        file = row[0]
        arguments.append([file])

    try:

        # submit a job order and retrieve job order id
        job_order_id: int = client.submit(
            joborder=JobOrderSubmit(
                command=[
                    '/home1/datahome/cerint/.conda/envs/felyx_processor/bin/felyx-extraction',
                    '-c',
                    '/home1/datahome/cerint/candre/felyx_data_maxss.yaml',
                    '--dataset_id',
                    'SEALEVEL_GLO_PHY_L3_REP_OBSERVATIONS_008_062_S3A',
                    '--miniprod_dir',
                    '/home1/datahome/cerint/candre/data/',
                    '--manifest_dir',
                    '/home1/datahome/cerint/candre/manifests/',
                    '--inputs',
                ],
                arguments=arguments,
                cores=1,
                split='/10',
                memory='1G',
                walltime='02:00:00',
                connection='ifrhtcondor',
                job_extra=[
                    # we have a small cluster
                    'n_workers=2',
                ]
            ),
            token=token,
        )

        print(job_order_id)

    except JobardError as error:
        print('*' * 70)
        print(error)


if __name__ == '__main__':
    my_test()
