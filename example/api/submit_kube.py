"""LOCAL API Submit test."""

from jobard_client.client import JobardClient
from jobard_client.core.exceptions import JobardError
from jobard_client.core.jobs.models import JobOrderSubmit


def my_test():
    """LOCAL API Submit test."""

    client = JobardClient('http://localhost:8000')
    token = 'access_token'

    try:

        # submit a job order and retrieve job order id
        job_order_id: int = client.submit(
            joborder=JobOrderSubmit(
                command=[
                    'echo',
                ],
                arguments=[['1'], ['2'], ['3']],
                cores=1,
                memory='500M',
                connection='kubernetes',
                job_extra=[
                    'n_workers=1',
                    'image=gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest',
                    'log_chroot=/mnt/k8s/data/'
                ],
            ),
            token=token,
        )

        print(job_order_id)

    except JobardError as error:
        print('*' * 70)
        print(error)


if __name__ == '__main__':
    my_test()
