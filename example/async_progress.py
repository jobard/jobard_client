# flake8: noqa
import asyncio
from typing import List, Optional

import tqdm

from jobard_client.client import JobardClient
from jobard_client.core.jobs.models import JobOrderStat


async def progress(jobard_client: JobardClient, job_order_id: int, position: int, token: str) -> JobOrderStat:
    final_state: Optional[JobOrderStat] = None

    progressbar = tqdm.tqdm(
        desc='Job order {0}'.format(job_order_id),
        total=100,
        leave=False,
        position=position,
    )

    percentage = 0
    async for jos in jobard_client.async_progress(job_order_id=job_order_id, token=token, delay_beetween_loops=1):
        await asyncio.sleep(1)
        new_percentage = int(jos.progress.percentage * 100)
        progressbar.update(new_percentage - percentage)
        percentage = new_percentage
        final_state = jos
    return final_state


async def timeout_many(timeout: int):
    elapsed = 0
    while elapsed < timeout:
        await asyncio.sleep(1)
        elapsed += 1
    raise TimeoutError()


async def progress_many(jobard_client: JobardClient, job_orders: List[int], token: str):
    tasks = [
        progress(jobard_client=jobard_client, job_order_id=jo, position=idx, token=token)
        for idx, jo in enumerate(job_orders)
    ]
    tasks.append(timeout_many(1000))
    return await asyncio.gather(*tasks)

client = JobardClient('http://localhost:8000/')
token = 'access_token'
res = asyncio.get_event_loop().run_until_complete(
    progress_many(jobard_client=client, job_orders=[11], token=token)
)

for item in res:
    print(item)
