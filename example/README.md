# Jobard client local usage

- start the jobard stack

```bash
docker stack deploy -c jobard-compose.yaml jobard
```

- check the stack (replicas should be `1/1`)

```bash
docker service ls --filter name=jobard
```

- edit and run the `sample.py` file

- remove the stack

```bash
docker stack rm jobard
```
