#  noqa: WPS202, WPS202
"""Jobard client exceptions."""
from jobard_client.core.exceptions import JobardError


class UnknownJobOrderError(JobardError):
    """Unknown JobOrder ID error."""

    def __init__(self, joborder_id: int):
        """Initialize exception.

        Args:
            joborder_id: JobOrder ID
        """
        super().__init__('Unknown job order {0}'.format(joborder_id))


class UnknownJobError(JobardError):
    """Unknown Job ID error."""

    def __init__(self, job_id: int):
        """Initialize exception.

        Args:
            job_id: JobOrder ID
        """
        super().__init__('Unknown job {0}'.format(job_id))


class ForbiddenParentJobOrderError(JobardError):
    """Forbidden job's parent job order error."""

    def __init__(self):
        """Initialize exception.

        """
        super().__init__('Job\'s parent job order does not belong to the authenticated user')


class ForbiddenJobOrderError(JobardError):
    """Forbidden job order error."""

    def __init__(self, job_order_id: int):
        """Initialize exception.

        Args:
            job_order_id: Job order ID
        """
        super().__init__('Job order ID {0} does not belong to the authenticated user'.format(job_order_id))


class AlreadyRegisteredDockerImageError(JobardError):
    """Already registered Docker image error."""

    def __init__(self, image_name: str):
        """Initialize exception.

        Args:
            image_name: Docker image name
        """
        super().__init__('Docker image name : {0} already registered for the user'.format(image_name))


class AlreadyRegisteredDockerMountPointError(JobardError):
    """Already registered Docker mount point error."""

    def __init__(self, mount_point_name: str):
        """Initialize exception.

        Args:
            mount_point_name: Docker moutn point name
        """
        super().__init__('Docker mount point name : {0} already registered for the user'.format(mount_point_name))


class UnknownDockerImageError(JobardError):
    """Unknown Docker image ID error."""

    def __init__(self, image_id: int):
        """Initialize exception.

        Args:
            image_id: Docker image  ID
        """
        super().__init__('Unknown Docker image {0}'.format(image_id))


class UnknownDockerMountPointError(JobardError):
    """Unknown Docker mount point ID error."""

    def __init__(self, mount_point_id: int):
        """Initialize exception.

        Args:
            mount_point_id: Docker mount point ID
        """
        super().__init__('Unknown Docker mount point {0}'.format(mount_point_id))


class ForbiddenDockerImageError(JobardError):
    """Forbidden Docker image error."""

    def __init__(self, docker_image_id: int):
        """Initialize exception.

        Args:
            docker_image_id: Docker image ID
        """
        super().__init__('Docker image ID {0} does not belong to the authenticated user'.format(docker_image_id))


class ForbiddenParentDockerImageError(JobardError):
    """Forbidden parent Docker image error."""

    def __init__(self):
        """Initialize exception."""
        super().__init__('Mount point parent Docker image does not belong to the authenticated user')


class CancellationUnauthorizedError(JobardError):
    """Unknown JobOrder ID error."""

    def __init__(self, joborder_id: int):
        """Initialize exception.

        Args:
            joborder_id: JobOrder ID
        """
        super().__init__('Cancellation unauthorized for job order {0}'.format(joborder_id))


class InvalidFieldError(JobardError):
    """Invalid field error."""
