"""JobOrder DTO."""
from __future__ import annotations

import json
from datetime import timedelta
from typing import List, Optional, Generic

from pydantic import BaseModel

try:
    import click
    import pandas as pd
    from tabulate import tabulate
    HAS_RENDERERS = True
except ImportError:
    HAS_RENDERERS = False

from jobard_client.core.models import JobardModel, Visitor, VisitorFactory, jobard_model_type
from jobard_client.core.core_types import Cluster, Format, HealthCheckStatus


class MonitoringResultEntity(JobardModel):
    """Monitoring entity output information."""

    alias: str
    status: Optional[HealthCheckStatus] = None
    time_taken: Optional[timedelta] = None
    tags: List[str]

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass


class MonitoringResultDatabaseEntity(MonitoringResultEntity):
    """Monitoring database output information."""


class MonitoringResultConnectionEntity(MonitoringResultEntity):
    """Monitoring host connection output information."""

    host_connection: str
    preemption_time: Optional[timedelta] = None


class MonitoringResultDaemonEntity(MonitoringResultEntity):
    """Monitoring daemon output information."""

    daemon_id: str
    host_connections: List[MonitoringResultConnectionEntity]


class MonitoringResult(JobardModel):
    """The global monitoring output information."""

    status: Optional[HealthCheckStatus] = None
    total_time_taken: Optional[timedelta] = None
    database_entity: MonitoringResultDatabaseEntity = None
    daemon_entities: List[MonitoringResultDaemonEntity] = None

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Accept operation for visiting monitoring object.
       Args:
           visitor : The monitoring visitor.
       """
        visitor.visit(self)


class RemoteConnectionInput(JobardModel):
    """Remote connection DTO."""

    name: str
    cluster_id: int
    daemon_id: Optional[str] = '0'

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass

class RemoteConnection(RemoteConnectionInput):
    """Remote connection DTO."""

    enabled: bool = True

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Accept operation for visiting monitoring object.
        Args:
            visitor : The monitoring visitor.
        """
        visitor.visit(self)

class RemoteConnectionUpdate(JobardModel):
    """Remote connection DTO."""

    name: str
    cluster_id: int
    daemon_id: Optional[str] = '0'
    enabled: bool = True

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass


class RemoteConnectionListModel(BaseModel, Generic[jobard_model_type]):
    """List of connections model."""

    records: Optional[List[jobard_model_type]] = None


class RemoteConnectionList(RemoteConnectionListModel[RemoteConnection]):
    """List of host connections DTO."""

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Accept operation for visiting monitoring object.
        Args:
            visitor : The monitoring visitor.
        """
        visitor.visit(self)


class JobardClusterInput(JobardModel):
    cluster_name: str
    type: Optional[Cluster] = Cluster.LOCAL
    healthcheck_interval: timedelta
    remote_username: Optional[str] = None
    password: Optional[str] = None
    client_key: Optional[str] = None

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass


class JobardCluster(JobardClusterInput):
    """Jobard cluster DTO."""
    cluster_id: int
    enabled: bool = True

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Accept operation for visiting monitoring object.
        Args:
            visitor : The monitoring visitor.
        """
        visitor.visit(self)

class JobardClusterUpdate(JobardModel):
    """Jobard cluster DTO."""

    cluster_id: int
    cluster_name: str
    type: Optional[Cluster] = Cluster.LOCAL
    healthcheck_interval: timedelta
    remote_username: Optional[str] = None
    password: Optional[str] = None
    client_key: Optional[str] = None
    enabled: bool = True

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass


class JobardClusterListModel(BaseModel, Generic[jobard_model_type]):
    """List of jobard clusters model."""

    records: Optional[List[jobard_model_type]] = None


class JobardClusterList(JobardClusterListModel[JobardCluster]):
    """List of jobard clusters DTO."""

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Accept operation for visiting monitoring object.
        Args:
            visitor : The monitoring visitor.
        """
        visitor.visit(self)


class RemoteConnectionClusterInput(JobardModel):
    """Remote connection + cluster DTO."""

    name: str
    daemon_id: Optional[str] = '0'
    cluster: JobardClusterInput

    def accept(self, visitor: MonitoringVisitor) -> None:
        """Do not implement """
        pass


class MonitoringVisitor(Visitor):
    """The Visitor Interface declares a set of visiting methods that correspond to component classes.
    The signature of a visiting method allows the visitor to identify the exact class of the component
    that it's dealing with.
    """

    def visit_monitoringresult(self, component: MonitoringResult) -> None:
        """Visiting method for monitoring result.

        Args:
            component: The monitoring result to visit.
        """
        pass

    def visit_remoteconnectionlist(self, component: RemoteConnectionList) -> None:
        """Visiting method for connections list.

        Args:
            component: The connection list to visit.
        """
        pass

    def visit_remoteconnection(self, component: RemoteConnection) -> None:
        """Visiting method for a remote connection.
        Args:
            component: The remote connection to visit.
        """
        pass

    def visit_jobardclusterlist(self, component: JobardClusterList) -> None:
        """Visiting method for clusters' list.

        Args:
            component: The cluster list to visit.
        """
        pass

    def visit_jobardcluster(self, component: JobardCluster) -> None:
        """Visiting method for a cluster.

        Args:
            component: The cluster to visit.
        """
        pass


"""
Concrete Visitors implement several formats for the output, which can
work with all concrete component classes.
"""


class JSONMonitoringVisitor(MonitoringVisitor):
    """This visitor implements the JSON output format."""

    def visit_monitoringresult(self, component: MonitoringResult) -> None:
        """Visiting method for monitoring result.

        Args:
            component: The monitoring result to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_remoteconnectionlist(self, component: RemoteConnectionList) -> None:
        """Visiting method for connections list.

        Args:
            component: The connection list to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_remoteconnection(self, component: RemoteConnection) -> None:
        """Visiting method for a remote connection.
        Args:
            component: The remote connection to visit.
        """
        click.echo(component.json())

    def visit_jobardclusterlist(self, component: JobardClusterList) -> None:
        """Visiting method for clusters' list.

        Args:
            component: The cluster list to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_jobardcluster(self, component: JobardCluster) -> None:
        """Visiting method for a Jobard cluster.
        Args:
            component: The cluster to visit.
        """
        click.echo(component.json())


class CSVMonitoringVisitor(MonitoringVisitor):
    """This visitor implements the CSV output format."""

    def visit_monitoringresult(self, component: MonitoringResult) -> None:
        """Visiting method for monitoring result.

        Args:
            component: The monitoring result to visit.
        Note:
            Not implemented for monitoring information. """

    def visit_remoteconnectionlist(self, component: RemoteConnectionList) -> None:
        """Visiting method for connections list.

        Args:
            component: The connection list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[record.dict() for record in component.records])
            click.echo(df.to_csv(index=False))

    def visit_remoteconnection(self, component: RemoteConnection) -> None:
        """Visiting method for a remote connection.
        Args:
            component: The remote connection to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

    def visit_jobardclusterlist(self, component: JobardClusterList) -> None:
        """Visiting method for clusters' list.

        Args:
            component: The cluster list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[record.dict() for record in component.records])
            click.echo(df.to_csv(index=False))

    def visit_jobardcluster(self, component: JobardCluster) -> None:
        """Visiting method for a Jobard cluster.
        Args:
            component: The cluster to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))


class TabularMonitoringVisitor(MonitoringVisitor):
    """
    This visitor implements the tabular output format.
    """

    def visit_monitoringresult(self, component: MonitoringResult) -> None:
        """Visiting method for monitoring result.

        Args:
            component: The monitoring result to visit.
        Note:
            Not implemented for monitoring information. """

    def visit_remoteconnectionlist(self, component: RemoteConnectionList) -> None:
        """Visiting method for connections list.

        Args:
            component: The connection list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[connection.dict() for connection in component.records])
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_remoteconnection(self, component: RemoteConnection) -> None:
        """Visiting method for a remote connection.
        Args:
            component: The remote connection to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_jobardclusterlist(self, component: JobardClusterList) -> None:
        """Visiting method for clusters' list.

        Args:
            component: The cluster list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[cluster.dict() for cluster in component.records])
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_jobardcluster(self, component: JobardCluster) -> None:
        """Visiting method for a Jobard cluster.
        Args:
            component: The cluster to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))


class MonitoringVisitorFactory(VisitorFactory):
    @classmethod
    def create_visitor(
        cls,
        output_format: Optional[Format] = Format.TABLE
    ) -> MonitoringVisitor:
        """Create a visitor according to the output format.

        Args:
           output_format: The output format (csv,json,table)
        Returns:
           the visitor
        """
        if HAS_RENDERERS is True:
            if output_format == Format.TABLE:
                return TabularMonitoringVisitor()

            if output_format == Format.JSON:
                return JSONMonitoringVisitor()

            if output_format == Format.CSV:
                return CSVMonitoringVisitor()
        else:
            return MonitoringVisitor()
