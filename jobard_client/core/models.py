"""Core DTO."""
from __future__ import annotations

from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import TypeVar

from pydantic import BaseModel, ConfigDict


class JobardModel(BaseModel):
    """Base Jobard model."""

    @abstractmethod
    def accept(self, visitor: Visitor) -> None:
        """ This method must be overwritten in the subclasses needing to format their output.
        They have to call `visitSubclass`, which matches the current class name.
        This way we let the visitor know the class of the component it works with.

        Args:
            visitor: The visitor used by the subclasses.
        """

    model_config = ConfigDict(
        validate_default=True,
        validate_assignment=True,
        json_encoders= {
            datetime: lambda date: date.isoformat(),
            timedelta: lambda time: str(time),
        }
    )


jobard_model_type = TypeVar('jobard_model_type', bound=JobardModel)


class Message(JobardModel):
    """API Message model."""

    detail: str

    def accept(self, visitor: Visitor) -> None:
        pass


class Visitor(ABC):
    """ The Visitor Interface declares a set of visiting methods that correspond to component classes.
    The signature of a visiting method allows the visitor to identify the exact class of the component
    that it's dealing with.
    """

    def visit(self, component: BaseModel) -> None:
        """
        Calls the component visit method according to the component class.
        Args:
            component: The component to visit.

        """
        visit_method = getattr(
            self,
            'visit_{0}'.format(type(component).__name__.lower())
        )
        try:
            visit_method(component)
        except AttributeError:
            raise AttributeError('{0} not implemented in {1}'.format(visit_method.__name__, type(self).__name__))


class VisitorFactory(ABC):
    """Base class of visitor factories."""
