"""Jobard core types."""

from enum import Enum
from typing import Annotated
from typing import List

from pydantic import Field

Arguments = List[str]

MEMORY_PATTERN = r'\d+[GMK]'
WALLTIME_PATTERN = r'^\d{2}:\d{2}:\d{2}$'
SPLIT_PATTERN = r'\/?[1-9]\d*'
COMMAND_ALIAS_PATTERN = r'^[A-Za-z0-9_-]*$'
JOB_ORDER_NAME_PATTERN = r'^[A-Za-z0-9_-]*$'

MemoryString = Annotated[str, Field(pattern=MEMORY_PATTERN)]
WalltimeString = Annotated[str, Field(pattern=WALLTIME_PATTERN)]
SplitString = Annotated[str, Field(pattern=SPLIT_PATTERN)]
CommandAlias = Annotated[str, Field(pattern=COMMAND_ALIAS_PATTERN)]
JobOrderName = Annotated[str, Field(pattern=JOB_ORDER_NAME_PATTERN)]

class Cluster(str, Enum):
    """List of clusters."""

    LOCAL = 'LOCAL'
    SWARM = 'SWARM'
    PBS = 'PBS'
    HTCONDOR = 'HTCONDOR'
    KUBE = 'KUBE'


class State(str, Enum):
    """List of states."""

    INIT_ASKED = 'INIT_ASKED'
    INIT_RUNNING = 'INIT_RUNNING'
    NEW = 'NEW'
    ENQUEUED = 'ENQUEUED'
    SUBMITTED = 'SUBMITTED'
    RUNNING = 'RUNNING'
    SUCCEEDED = 'SUCCEEDED'
    FINISHED_AND_LOGS_PARSED = 'FINISHED_AND_LOGS_PARSED'
    FAILED = 'FAILED'
    CANCELLATION_ASKED = 'CANCELLATION_ASKED'
    CANCELLED = 'CANCELLED'


states_closed = (State.SUCCEEDED, State.CANCELLED, State.FAILED)


class Format(str, Enum):
    """List of output formats."""
    JSON = 'JSON'
    CSV = 'CSV'
    TABLE = 'TABLE'


class StateColor:
    """Class used to associate an ANSI color to a state for console output """
    ANSI_COLORS = {
        'white': '\033[0m',
        'red': '\033[31m',
        'green': '\033[32m',
        'yellow': '\033[33m',
        'blue': '\033[34m',
        'purple': '\033[35m',
        'cyan': '\033[36m',
        'reset': '\033[0m'
    }

    STATE_COLORS = {
        State.INIT_ASKED: ANSI_COLORS['white'],
        State.INIT_RUNNING: ANSI_COLORS['white'],
        State.NEW: ANSI_COLORS['white'],
        State.ENQUEUED: ANSI_COLORS['white'],
        State.SUBMITTED: ANSI_COLORS['white'],
        State.RUNNING: ANSI_COLORS['white'],
        State.SUCCEEDED: ANSI_COLORS['green'],
        State.FINISHED_AND_LOGS_PARSED: ANSI_COLORS['green'],
        State.FAILED: ANSI_COLORS['red'],
        State.CANCELLATION_ASKED: ANSI_COLORS['white'],
        State.CANCELLED: ANSI_COLORS['yellow']
    }

    @classmethod
    def get_color(cls, state: State) -> str:
        """
        Get the color of the state.
        Args:
            state: The state.
        Returns:
            The ANSI color.
        """
        return cls.STATE_COLORS[state]

    @classmethod
    def reset_color(cls) -> str:
        """
        Reset the color (turn to white color).
        Returns:
            The initial color (white).
        """
        return cls.ANSI_COLORS['reset']

    @classmethod
    def colorize(cls, state: State) -> str:
        """
        Set the color code for the state.
        Args:
            state: The state to colorize.
        Returns:
            The state value with the color code.
        """
        return StateColor.get_color(state) + state.format('%s') + StateColor.reset_color()


class HealthCheckStatus(str, Enum):
    """List of healthcheck status."""

    HEALTHY = 'Healthy'
    UNHEALTHY = 'Unhealthy'
