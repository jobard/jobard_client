"""Generic exceptions."""


class JobardError(Exception):
    """Jobard generic error."""
