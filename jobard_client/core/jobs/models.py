"""JobOrder DTO."""
from __future__ import annotations

import json
from abc import abstractmethod
from datetime import datetime
from typing import Generic, List, Optional, Dict

from pydantic import BaseModel, field_validator
from pydantic_core.core_schema import ValidationInfo

from jobard_client.exceptions import InvalidFieldError

try:
    import click
    import pandas as pd
    from tabulate import tabulate
    HAS_RENDERERS = True
except ImportError:
    HAS_RENDERERS = False

from jobard_client.core.models import JobardModel, Visitor, VisitorFactory, jobard_model_type
from jobard_client.core.core_types import Arguments, Format, State, StateColor, MemoryString, SplitString, \
    WalltimeString, CommandAlias, JobOrderName


class JobardListModel(BaseModel, Generic[jobard_model_type]):
    """List of jobs DTO."""

    count: int = 0
    offset: int = 0
    limit: int = 0
    records: Optional[List[jobard_model_type]] = None

    @abstractmethod
    def accept(self, visitor: JobVisitor) -> None:
        pass


class JobOrderSubmit(JobardModel):
    """Job order submitted DTO."""
    name: Optional[JobOrderName] = None
    command: List[str]
    arguments: Optional[List[Arguments]] = None
    interpolate_command: Optional[bool] = False
    split: Optional[SplitString] = None
    priority: Optional[int] = 1
    cores: Optional[int] = 1
    memory: Optional[MemoryString] = '512M'
    walltime: Optional[WalltimeString] = '00:05:00'
    max_concurrency: Optional[int] = 1
    connection: Optional[str] = None
    job_extra: Optional[List[str]] = None
    env_extra: Optional[Dict[str, str]] = None
    user_id: Optional[int] = None
    image_id: Optional[int] = None
    command_alias: Optional[CommandAlias] = None
    job_log_prefix_arg_idx: Optional[int] = None

    @field_validator('job_log_prefix_arg_idx')
    def job_log_prefix_arg_idx_validator(cls, v: int, info: ValidationInfo):
        if v is not None:
            args = info.data['arguments'] if 'arguments' in info.data else []
            max_idx = len(min(args, key=len))-1
            if v < 0 or v > max_idx:
                raise InvalidFieldError(
                    f'Invalid job log prefix argument index value, must be a value between 0 and {max_idx}')
            return v

    def accept(self, visitor: JobVisitor) -> None:
        pass


class JobOrder(JobOrderSubmit):
    """Job order DTO."""

    id: int
    state: Optional[str] = State.INIT_ASKED
    progress: Optional[float] = 0
    job_count: Optional[int] = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    concurrency: Optional[int] = 0
    root_log_path: Optional[str] = None
    is_healthcheck: Optional[bool] = False

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobArray(JobardModel):
    """JobArray DTO."""

    id: int
    job_order: int
    job_offset: int
    job_count: int
    scheduled_id: Optional[int]
    state: State = State.NEW
    progress: float = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    daemon_id: Optional[str] = None
    executor_id: Optional[str] = None
    executor_log_path: Optional[str] = None
    error_message: Optional[str] = None
    try_nb: int

    def accept(self, visitor: JobVisitor) -> None:
        pass


class Job(JobardModel):
    """Job DTO."""

    id: int
    job_array: int
    job_array_index: int
    command_with_parameters: List[str]
    arguments: Arguments = None
    scheduled_id: Optional[str] = None
    state: Optional[str] = State.NEW
    progress: Optional[float] = 0
    created: Optional[datetime] = datetime.utcnow()
    updated: Optional[datetime] = datetime.utcnow()
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    error_message: Optional[str] = None
    log_path_stdout: Optional[str] = None
    log_path_stderr: Optional[str] = None

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class StatProgress(JobardModel):
    """JobOrder progress DTO."""

    percentage: float = 0
    job_states: dict = None

    def accept(self, visitor: JobVisitor) -> None:
        pass


class JobOrderStat(JobardModel):
    """JobOrder statistics DTO."""

    id: int
    name: str = None
    state: str
    created: Optional[datetime] = None
    updated: Optional[datetime] = None
    submitted: Optional[datetime] = None
    finished: Optional[datetime] = None
    job_count: int = 0
    progress: Optional[StatProgress] = None
    error_message: Optional[str] = None

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobOrderList(JobardListModel[JobOrder]):
    """List of job orders DTO."""

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobList(JobardListModel[Job]):
    """List of jobs DTO."""

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobArgList(JobardListModel[Arguments]):
    """List of job arguments DTO."""

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobArrayList(JobardListModel[JobArray]):
    """List of job arrays DTO."""

    def accept(self, visitor: JobVisitor) -> None:
        """Accept operation for visiting job object.
        Args:
           visitor : The job visitor.
        """
        visitor.visit(self)


class JobVisitor(Visitor):
    """The Visitor Interface declares a set of visiting methods that correspond to component classes.
    The signature of a visiting method allows the visitor to identify the exact class of the component
    that it's dealing with.
    """

    def visit_jobarraylist(self, component: JobArrayList) -> None:
        """Visiting method for job array list.
        Args:
            component: The job array list to visit.
        """
        pass

    def visit_job(self, component: Job) -> None:
        """Visiting method for job.
        Args:
            component: The job to visit.
        """
        pass

    def visit_jobarglist(self, component: JobArgList) -> None:
        """Visiting method for job argument list.
        Args:
            component: The job argument list to visit.
        """
        pass

    def visit_joblist(self, component: JobList) -> None:
        """Visiting method for job list.
        Args:
            component: The job list to visit.
        """
        pass

    def visit_joborder(self, component: JobOrder) -> None:
        """Visiting method for job order.
        Args:
            component: The job order to visit.
        """
        pass

    def visit_joborderlist(self, component: JobOrderList) -> None:
        """Visiting method for job order list.
        Args:
            component: The job order list to visit.
        """

    def visit_joborderstat(self, component: JobOrderStat) -> None:
        """Visiting method for job order stats.
        Args:
            component: The job order stats to visit.
        """
        pass


"""
Concrete Visitors implement several formats for the output, which can
work with all concrete component classes.
"""


class JSONJobVisitor(JobVisitor):
    """This visitor implements the JSON output format."""

    def visit_jobarraylist(self, component: JobArrayList) -> None:
        """Visiting method for job array list.
        Args:
            component: The job array list to visit.
        """
        for job_array in component.records:
            click.echo(job_array.model_dump_json())
        # TODO CR ou plus structurée  :
        # df = pd.DataFrame(data=[job_array.dict() for job_array in component.records])
        # parsed = json.loads(df.to_json(orient='table'))
        # click.echo(json.dumps(parsed['data']))

    def visit_job(self, component: Job) -> None:
        """Visiting method for job.
        Args:
            component: The job to visit.
        """
        click.echo(component.model_dump_json())

    def visit_jobarglist(self, component: JobArgList) -> None:
        """Visiting method for job argument list.
        Args:
            component: The job argument list to visit.
        """
        args_dict = dict()
        args_dict['args'] = component.records
        click.echo(json.dumps(args_dict))

    def visit_joblist(self, component: JobList) -> None:
        """Visiting method for job list.
        Args:
            component: The job list to visit.
        """
        for job in component.records:
            click.echo(job.model_dump_json())

    def visit_joborder(self, component: JobOrder) -> None:
        """Visiting method for joborder list.
        Args:
            component: The joborder to visit.
        """
        click.echo(component.model_dump_json())

    def visit_joborderlist(self, component: JobOrderList) -> None:
        """Visiting method for job order list.
        Args:
            component: The job order list to visit.
        """
        for job_order in component.records:
            click.echo(job_order.model_dump_json())

    def visit_joborderstat(self, component: JobOrderStat) -> None:
        """Visiting method for job order stats.
        Args:
            component: The job order stats to visit.
        """
        click.echo(component.model_dump_json())


class TabularJobVisitor(JobVisitor):
    """This visitor implements the TABLE output format."""

    def visit_jobarraylist(self, component: JobArrayList) -> None:
        """Visiting method for job array list.
        Args:
            component: The job array list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_array.model_dump() for job_array in component.records])
            # TODO CR Faut-il filtrer les colonnes?
            df = df[['id', 'state', 'job_order', 'executor_log_path', 'error_message', 'try_nb']]
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_job(self, component: Job) -> None:
        """Visiting method for job.
        Args:
            component: The job to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_jobarglist(self, component: JobArgList) -> None:
        """Visiting method for job argument list.
        Args:
            component: The job argument list to visit.
        """
        if len(component.records) > 0:
            cols = ['arg{}'.format(idx) for idx in range(len(component.records[0]))]
            df = pd.DataFrame(columns=cols, data=component.records)
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_joblist(self, component: JobList) -> None:
        """Visiting method for job list.
        Args:
            component: The job list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_array.model_dump() for job_array in component.records])
            # TODO CR Faut-il filtrer les colonnes?
            df = df[['id', 'job_array', 'state', 'command_with_parameters', 'arguments', 'log_path_stdout']]
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_joborder(self, component: JobOrder) -> None:
        """Visiting method for job order list.
        Args:
            component: The job array order to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        # TODO CR Faut-il filtrer les colonnes?
        df = df[
            [
                'name',
                'image_id',
                'is_healthcheck',
                'command',
                'arguments',
                'state',
                'progress',
                'connection',
                'job_count',
                'created',
                'submitted',
                'finished',
                'command_alias',
                'job_log_prefix_arg_idx',
                'user_id',
            ]
        ]
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_joborderlist(self, component: JobOrderList) -> None:
        """Visiting method for job order list.
        Args:
            component: The job order list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_order.model_dump() for job_order in component.records])
            # TODO CR Faut-il filtrer les colonnes?
            df = df[['id', 'name', 'image_id', 'is_healthcheck', 'state', 'job_count', 'progress', 'user_id']]
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_joborderstat(self, component: JobOrderStat) -> None:
        """Visiting method for job order stats.
        Args:
            component: The job order stats to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        # TODO CR Voir si on affiche la colonne progress
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))


class CSVJobVisitor(JobVisitor):
    """This visitor implements the CSV output format."""

    def visit_jobarraylist(self, component: JobArrayList) -> None:
        """Visiting method for job array list.
        Args:
            component: The job array list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_array.model_dump() for job_array in component.records])
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(df.to_csv(index=False))

    def visit_job(self, component: Job) -> None:
        """Visiting method for job.
        Args:
            component: The job to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(df.to_csv(index=False))

    def visit_jobarglist(self, component: JobArgList) -> None:
        """Visiting method for job argument list.
        Args:
            component: The job argument list to visit.
        """
        if len(component.records) > 0:
            cols = ['arg{}'.format(idx) for idx in range(len(component.records[0]))]
            df = pd.DataFrame(columns=cols, data=component.records)
            click.echo(df.to_csv(index=False))

    def visit_joblist(self, component: JobList) -> None:
        """Visiting method for job list.
        Args:
            component: The job list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_array.model_dump() for job_array in component.records])
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(df.to_csv(index=False))

    def visit_joborder(self, component: JobOrder) -> None:
        """Visiting method for job order.
        Args:
            component: The job order to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(df.to_csv(index=False))

    def visit_joborderlist(self, component: JobOrderList) -> None:
        """Visiting method for job order list.
        Args:
            component: The job order list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[job_order.model_dump() for job_order in component.records])
            df['state'] = df['state'].apply(StateColor.colorize)
            click.echo(df.to_csv(index=False))

    def visit_joborderstat(self, component: JobOrderStat) -> None:
        """Visiting method for job order stats.
        Args:
            component: The job order stats to visit.
        """
        df = pd.DataFrame(data=[component.model_dump()])
        # TODO CR Voir si on affiche la colonne progress
        df['state'] = df['state'].apply(StateColor.colorize)
        click.echo(df.to_csv(index=False))


class JobVisitorFactory(VisitorFactory):
    @classmethod
    def create_visitor(
        cls,
        output_format: Format
    ) -> JobVisitor:
        """Create a visitor according to the output format.

        Args:
           output_format: The output format (csv,json,table)
        Returns:
           the visitor
        """
        if HAS_RENDERERS is True:
            if output_format == Format.TABLE:
                return TabularJobVisitor()

            if output_format == Format.JSON:
                return JSONJobVisitor()

            if output_format == Format.CSV:
                return CSVJobVisitor()
        else:
            return JobVisitor()
