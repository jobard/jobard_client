"""Docker images and mount points DTO."""
from __future__ import annotations

from abc import abstractmethod
from typing import Generic, List, Optional
from pydantic import BaseModel

try:
    import click
    import pandas as pd
    from tabulate import tabulate
    HAS_RENDERERS = True
except ImportError:
    HAS_RENDERERS = False

from jobard_client.core.models import JobardModel, Visitor, VisitorFactory, jobard_model_type
from jobard_client.core.core_types import Format


class JobardListModel(BaseModel, Generic[jobard_model_type]):
    """List of jobard DTO."""

    count: int = 0
    offset: int = 0
    limit: int = 0
    records: Optional[List[jobard_model_type]] = None

    @abstractmethod
    def accept(self, visitor: DockerVisitor) -> None:
        pass


class DockerMountPointUpdate(JobardModel):
    """Docker mount point update DTO."""

    mount_point_id: int
    name: str = None
    source: str = None
    destination: str = None

    def accept(self, visitor: DockerVisitor) -> None:
        pass


class DockerMountPointInput(JobardModel):
    """Docker mount point input DTO."""
    image_id: int
    name: str
    source: str
    destination: str

    def accept(self, visitor: DockerVisitor) -> None:
        pass


class DockerMountPoint(DockerMountPointInput):
    """Docker mount point DTO."""
    mount_point_id: int

    def accept(self, visitor: DockerVisitor) -> None:
        """Accept operation for visiting Docker mount point object.
        Args:
           visitor : The Docker mount point visitor.
        """
        visitor.visit(self)


class DockerMountPointList(JobardListModel[DockerMountPoint]):
    """List of Docker mount points DTO."""

    def accept(self, visitor: DockerVisitor) -> None:
        """Accept operation for visiting Docker mount point object.
        Args:
           visitor : The Docker mount point visitor.
        """
        visitor.visit(self)


class DockerImageUpdate(JobardModel):
    """Docker image update DTO."""

    image_id: int
    name: str = None
    url: str = None
    is_test: bool

    def accept(self, visitor: DockerVisitor) -> None:
        pass


class DockerImageInput(JobardModel):
    """DockerImageInput DTO."""
    name: str
    url: str
    is_test: bool = False

    def accept(self, visitor: DockerVisitor) -> None:
        """Accept operation for visiting Docker image object.
        Args:
           visitor : The Docker image visitor.
        """
        pass


class DockerImage(DockerImageInput):
    """DockerImage DTO."""
    image_id: int
    user_id: int
    enabled: bool

    def accept(self, visitor: DockerVisitor) -> None:
        """Accept operation for visiting Docker image object.
        Args:
           visitor : The Docker image visitor.
        """
        visitor.visit(self)


class DockerImageList(JobardListModel[DockerImage]):
    """List of Docker  images DTO."""

    def accept(self, visitor: DockerVisitor) -> None:
        """Accept operation for visiting Docker image object.
        Args:
           visitor : The Docker image visitor.
        """
        visitor.visit(self)


class DockerVisitor(Visitor):
    """The Visitor Interface declares a set of visiting methods that correspond to component classes.
    The signature of a visiting method allows the visitor to identify the exact class of the component
    that it's dealing with.
    """

    def visit_dockerimage(self, component: DockerImage) -> None:
        """Visiting method for Docker image.
        Args:
            component: The Docker image to visit.
        """
        pass

    def visit_dockerimagelist(self, component: DockerImageList) -> None:
        """Visiting method for Docker image list.
        Args:
            component: The Docker image list to visit.
        """
        pass

    def visit_dockermountpoint(self, component: DockerMountPoint) -> None:
        """Visiting method for Docker mount point.
        Args:
            component: The Docker mount point to visit.
        """
        pass

    def visit_dockermountpointlist(self, component: DockerMountPointList) -> None:
        """Visiting method for Docker mount point list.
        Args:
            component: The Docker mount point list to visit.
        """
        pass


class JSONDockerVisitor(DockerVisitor):
    """This visitor implements the JSON output format."""

    def visit_dockerimage(self, component: DockerImage) -> None:
        """Visiting method for Docker image.
        Args:
            component: The Docker image to visit.
        """
        click.echo(component.json())

    def visit_dockerimagelist(self, component: DockerImageList) -> None:
        """Visiting method for Docker image  list.
        Args:
            component: The Docker image  list to visit.
        """
        for docker_image in component.records:
            click.echo(docker_image.json())

    def visit_dockermountpoint(self, component: DockerMountPoint) -> None:
        """Visiting method for Docker mount point.
        Args:
            component: The Docker mount point to visit.
        """
        click.echo(component.json())

    def visit_dockermountpointlist(self, component: DockerMountPointList) -> None:
        """Visiting method for Docker mount point list.
        Args:
            component: The Docker mount point list to visit.
        """
        for docker_mount_point in component.records:
            click.echo(docker_mount_point.json())


class TabularDockerVisitor(DockerVisitor):
    """This visitor implements the TABLE output format."""

    def visit_dockerimage(self, component: DockerImage) -> None:
        """Visiting method for Docker image.
        Args:
            component: The Docker image to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        df = df[['image_id', 'name', 'user_id', 'url', 'is_test']]
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_dockerimagelist(self, component: DockerImageList) -> None:
        """Visiting method for Docker image list.
        Args:
            component: The Docker image list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[docker_image.dict() for docker_image in component.records])
            df = df[['image_id', 'name', 'user_id', 'url', 'is_test']]
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_dockermountpoint(self, component: DockerMountPoint) -> None:
        """Visiting method for Docker mount point.
        Args:
            component: The Docker mount point to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        df = df[['mount_point_id', 'name', 'image_id', 'source', 'destination']]
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_dockermountpointlist(self, component: DockerMountPointList) -> None:
        """Visiting method for Docker mount point list.
        Args:
            component: The Docker mount point list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[docker_mount_point.dict() for docker_mount_point in component.records])
            df = df[['mount_point_id', 'name', 'image_id', 'source', 'destination']]
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))


class CSVDockerVisitor(DockerVisitor):
    """This visitor implements the CSV output format."""

    def visit_dockerimage(self, component: DockerImage) -> None:
        """Visiting method for Docker image.
        Args:
            component: The Docker image to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

    def visit_dockerimagelist(self, component: DockerImageList) -> None:
        """Visiting method for Docker image list.
        Args:
            component: The Docker image list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[docker_image.dict() for docker_image in component.records])
            click.echo(df.to_csv(index=False))

    def visit_dockermountpoint(self, component: DockerMountPoint) -> None:
        """Visiting method for Docker mount point.
        Args:
            component: The Docker mount point to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

    def visit_dockermountpointlist(self, component: DockerMountPointList) -> None:
        """Visiting method for Docker mount point list.
        Args:
            component: The Docker mount point list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[docker_mount_point.dict() for docker_mount_point in component.records])
            click.echo(df.to_csv(index=False))


class DockerVisitorFactory(VisitorFactory):
    @classmethod
    def create_visitor(
        cls,
        output_format: Format
    ) -> DockerVisitor:
        """Create a visitor according to the output format.

        Args:
           output_format: The output format (csv,json,table)
        Returns:
           the visitor
        """
        if HAS_RENDERERS is True:
            if output_format == Format.TABLE:
                return TabularDockerVisitor()

            if output_format == Format.JSON:
                return JSONDockerVisitor()

            if output_format == Format.CSV:
                return CSVDockerVisitor()
        else:
            return DockerVisitor()
