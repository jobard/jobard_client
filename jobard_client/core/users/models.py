"""JobardUser DTO."""
from __future__ import annotations

import json
from typing import List, Optional, Generic

try:
    import click
    import pandas as pd
    from tabulate import tabulate
    HAS_RENDERERS = True
except ImportError:
    HAS_RENDERERS = False

from pydantic import BaseModel

from jobard_client.core.models import JobardModel, Visitor, VisitorFactory, jobard_model_type
from jobard_client.core.core_types import Format


class JobardUserInput(JobardModel):
    username: str
    email: str
    remote_username: str
    password: Optional[str] = None
    client_key: Optional[str] = None
    unix_uid: Optional[int] = None

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class JobardUser(JobardUserInput):
    """Jobard user DTO."""
    user_id: int
    enabled: bool = True

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for visiting user object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)

class JobardUserUpdate(JobardModel):
    """Jobard user DTO."""

    user_id: int
    username: str
    email: str
    remote_username: str
    password: Optional[str] = None
    client_key: Optional[str] = None
    unix_uid: Optional[int] = None
    enabled: bool = True

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class JobardUserListModel(BaseModel, Generic[jobard_model_type]):
    """List of jobard users model."""

    records: Optional[List[jobard_model_type]] = None


class JobardUserList(JobardUserListModel[JobardUser]):
    """List of jobard users DTO."""

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for visiting user object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)


class ClusterAccessInput(JobardModel):
    """Cluster access DTO."""

    cluster_access_name: str
    user_id: int
    cluster_id: int
    log_root: Optional[str] = None
    remote_app_path: Optional[str] = None
    entry_point_wrapper: Optional[str] = None
    entry_point: Optional[str] = 'jobard-remote-dask'

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class ClusterAccess(ClusterAccessInput):
    """Cluster access DTO."""
    cluster_access_id: int
    enabled: bool = True

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for visiting user object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)

class ClusterAccessUpdate(JobardModel):
    """Cluster access DTO."""
    cluster_access_id: int
    cluster_access_name: str
    user_id: int
    cluster_id: int
    log_root: Optional[str] = None
    remote_app_path: Optional[str] = None
    entry_point_wrapper: Optional[str] = None
    entry_point: Optional[str] = 'jobard-remote-dask'

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class ClusterAccessListModel(BaseModel, Generic[jobard_model_type]):
    """List of cluster accesses model."""

    records: Optional[List[jobard_model_type]] = None


class ClusterAccessList(ClusterAccessListModel[ClusterAccess]):
    """List of cluster accesses DTO."""

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for user monitoring object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)

class AccessTokenInput(JobardModel):
    """Access token DTO."""

    user_id: int
    access_token: str

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class AccessToken(AccessTokenInput):
    """Access token DTO."""
    token_id: int
    enabled: bool = True

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for visiting user object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)

class AccessTokenUpdate(JobardModel):
    """Access token DTO."""
    user_id: int
    access_token: str
    token_id: int

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class AccessTokenListModel(BaseModel, Generic[jobard_model_type]):
    """List of access tokens model."""

    records: Optional[List[jobard_model_type]] = None


class AccessTokenList(AccessTokenListModel[AccessToken]):
    """List of access tokens DTO."""

    def accept(self, visitor: UserVisitor) -> None:
        """Accept operation for user monitoring object.
        Args:
            visitor : The user visitor.
        """
        visitor.visit(self)


class UserAccessInput(JobardModel):
    """User and token DTO."""
    user: JobardUserInput
    access_token: str

    def accept(self, visitor: UserVisitor) -> None:
        """Do not implement """
        pass


class UserVisitor(Visitor):
    """The Visitor Interface declares a set of visiting methods that correspond to component classes.
    The signature of a visiting method allows the visitor to identify the exact class of the component
    that it's dealing with.
    """

    def visit_jobarduserlist(self, component: JobardUserList) -> None:
        """Visiting method for users' list.

        Args:
            component: The user list to visit.
        """
        pass

    def visit_jobarduser(self, component: JobardUser) -> None:
        """Visiting method for a user.

        Args:
            component: The user to visit.
        """
        pass

    def visit_clusteraccesslist(self, component: ClusterAccessList) -> None:
        """Visiting method for cluster accesses' list.

        Args:
            component: The cluster accesses list to visit.
        """
        pass

    def visit_clusteraccess(self, component: ClusterAccess) -> None:
        """Visiting method for a cluster access.

        Args:
            component: The cluster access to visit.
        """
        pass

    def visit_accesstokenlist(self, component: AccessTokenList) -> None:
        """Visiting method for access tokens' list.

        Args:
            component: The access tokens list to visit.
        """
        pass

    def visit_accesstoken(self, component: AccessToken) -> None:
        """Visiting method for an access token.

        Args:
            component: The access token to visit.
        """
        pass

"""
Concrete Visitors implement several formats for the output, which can
work with all concrete component classes.
"""


class JSONUserVisitor(UserVisitor):
    """This visitor implements the JSON output format."""

    def visit_jobarduserlist(self, component: JobardUserList) -> None:
        """Visiting method for users' list.

        Args:
            component: The user list to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_jobarduser(self, component: JobardUser) -> None:
        """Visiting method for a Jobard user.
        Args:
            component: The user to visit.
        """
        click.echo(component.json())

    def visit_clusteraccesslist(self, component: ClusterAccessList) -> None:
        """Visiting method for cluster access' list.

        Args:
            component: The cluster access list to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_clusteraccess(self, component: ClusterAccess) -> None:
        """Visiting method for a Jobard cluster access.
        Args:
            component: The cluster access to visit.
        """
        click.echo(component.json())

    def visit_accesstokenlist(self, component: AccessTokenList) -> None:
        """Visiting method for access token' list.

        Args:
            component: The access token list to visit.
        """
        json_elt = json.loads(component.json())
        click.echo(json.dumps(json_elt, indent=2))

    def visit_accesstoken(self, component: AccessToken) -> None:
        """Visiting method for an access token.
        Args:
            component: The access token to visit.
        """
        click.echo(component.json())

class CSVUserVisitor(UserVisitor):
    """This visitor implements the CSV output format."""

    def visit_jobarduserlist(self, component: JobardUserList) -> None:
        """Visiting method for users' list.

        Args:
            component: The user list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[record.dict() for record in component.records])
            click.echo(df.to_csv(index=False))

    def visit_jobarduser(self, component: JobardUser) -> None:
        """Visiting method for a Jobard user.
        Args:
            component: The user to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

    def visit_clusteraccesslist(self, component: ClusterAccessList) -> None:
        """Visiting method for cluster access' list.

        Args:
            component: The cluster access list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[record.dict() for record in component.records])
            click.echo(df.to_csv(index=False))

    def visit_clusteraccess(self, component: ClusterAccess) -> None:
        """Visiting method for a Jobard cluster access.
        Args:
            component: The cluster access to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

    def visit_accesstokenlist(self, component: AccessTokenList) -> None:
        """Visiting method for access token' list.

        Args:
            component: The access token list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[record.dict() for record in component.records])
            click.echo(df.to_csv(index=False))

    def visit_accesstoken(self, component: AccessToken) -> None:
        """Visiting method for a Jobard access token.
        Args:
            component: The access token to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(df.to_csv(index=False))

class TabularUserVisitor(UserVisitor):
    """
    This visitor implements the tabular output format.
    """

    def visit_jobarduserlist(self, component: JobardUserList) -> None:
        """Visiting method for users' list.

        Args:
            component: The user list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[user.dict() for user in component.records])
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_jobarduser(self, component: JobardUser) -> None:
        """Visiting method for a Jobard user.
        Args:
            component: The user to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_clusteraccesslist(self, component: ClusterAccessList) -> None:
        """Visiting method for cluster access' list.

        Args:
            component: The cluster access list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[user.dict() for user in component.records])
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_clusteraccess(self, component: ClusterAccess) -> None:
        """Visiting method for a Jobard cluster access.
        Args:
            component: The cluster access to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_accesstokenlist(self, component: AccessTokenList) -> None:
        """Visiting method for access token' list.

        Args:
            component: The access token list to visit.
        """
        if len(component.records) > 0:
            df = pd.DataFrame(data=[user.dict() for user in component.records])
            click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

    def visit_accesstoken(self, component: AccessToken) -> None:
        """Visiting method for a Jobard access token.
        Args:
            component: The access token to visit.
        """
        df = pd.DataFrame(data=[component.dict()])
        click.echo(tabulate(df, headers='keys', tablefmt='psql', showindex=False))

class UserVisitorFactory(VisitorFactory):
    @classmethod
    def create_visitor(
        cls,
        output_format: Optional[Format] = Format.TABLE
    ) -> UserVisitor:
        """Create a visitor according to the output format.

        Args:
           output_format: The output format (csv,json,table)
        Returns:
           the visitor
        """
        if HAS_RENDERERS is True:
            if output_format == Format.TABLE:
                return TabularUserVisitor()

            if output_format == Format.JSON:
                return JSONUserVisitor()

            if output_format == Format.CSV:
                return CSVUserVisitor()
        else:
            return UserVisitor()
