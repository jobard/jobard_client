"""Jobard API client."""
import asyncio
import getpass
import json
import logging
import time
import urllib.parse
from datetime import datetime
from typing import AsyncIterable, Iterator, List, Optional, Tuple, Dict, Union

import requests

from jobard_client.cli.helpers import handle_token
from jobard_client.core.core_types import Arguments, State, states_closed
from jobard_client.core.docker import models as docker_models
from jobard_client.core.exceptions import JobardError
from jobard_client.core.jobs import models as job_models
from jobard_client.core.models import Message
from jobard_client.core.monitoring import models as monitoring_models
from jobard_client.core.users import models as users_models
from jobard_client.exceptions import (
    CancellationUnauthorizedError,
    UnknownJobError,
    UnknownJobOrderError,
    UnknownDockerImageError,
    UnknownDockerMountPointError,
    ForbiddenDockerImageError,
    ForbiddenJobOrderError,
    ForbiddenParentJobOrderError,
    ForbiddenParentDockerImageError,
    AlreadyRegisteredDockerImageError,
    AlreadyRegisteredDockerMountPointError, )

logger = logging.getLogger('jobard_client')


STATUS_OK = (200, 201, 204)
STATUS_UNKNOWN = (404,)
STATUS_ERROR = (400, 401)
STATUS_FORBIDDEN = (403,)
STATUS_FAILED_DEPENDENCY = (424,)
ENDPOINT_DOCKER_IMAGES = 'docker-images'
ENDPOINT_DOCKER_MOUNT_POINTS = 'docker-mount-points'
ENDPOINT_JOB_ORDERS = 'job-orders'
ENDPOINT_JOBS = 'jobs'
ENDPOINT_HEALTH = 'health'
ENDPOINT_CONNECTIONS = 'connections'
ENDPOINT_CLUSTERS = 'clusters'
ENDPOINT_USERS = 'users'
ENDPOINT_CLUSTER_ACCESSES = 'cluster-accesses'
ENDPOINT_ACCESS_TOKENS = 'access-tokens'
ENDPOINT_CONNECTION_CLUSTER = 'connections-cluster'
ENDPOINT_USER_ACCESS = 'user-accesses'
MESSAGE_PARSING_ERROR = 'Failed to parse the response from the API : {0}'
MESSAGE_RESPONSE_STATUS = 'Response status : {0}'


def _build_state_filter(state_list: List[State]) -> List[Tuple[str, str]]:
    """Build state filter.

    Args:
        state_list: The state list

    Returns:
        The list of states formatted as a list of tuples : ('state','<state_value').
    """

    return [('state', '{}'.format(item)) for item in state_list]


def _format_headers(token: str) -> Dict[str, str]:
    """Format request headers.

    Args:
        token: authentication token

    Returns:
        The headers' dictionary
    """
    return {'Authorization': 'Bearer {0}'.format(handle_token(cli_token=token))}


class JobardClient(object):  # noqa: WPS214
    """Jobard API client class."""

    def __init__(self, api: str = 'http://localhost:8000'):
        """Initialize class.

        Args:
            api: URL to the Jobard API
        """
        self._api_url = api.rstrip('/')

    def api_url(self):
        """Return the api url."""
        return self._api_url

    def submit(self, joborder: job_models.JobOrderSubmit, token: str) -> int:
        """Submit a job order.

        Args:
            joborder: joborder to submit
            token: authentication token

        Returns:
            The jobOrder id

        Raises:
            JobardError: error while submitting job order
        """

        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_JOB_ORDERS),
            headers=_format_headers(token=token),
            data=joborder.json(),
        )
        try:
            response.raise_for_status()
            return int(response.text)
        except requests.HTTPError as error:
            raise JobardError('Error while submitting the job order : {0}'.format(error.response.text))

    def job_order(self, job_order_id: int, token: str) -> job_models.JobOrder:
        """Retrieve a job order.

        Args:
            job_order_id: job_order_id
            token: authentication token

        Returns:
            the job order instance

        Raises:
            UnknownJobError: If the job id is unknown
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_JOB_ORDERS, job_order_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return job_models.JobOrder.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while retrieving the job order : {0}'.format(error))

    def cancel(self, job_order_id: int, token: str) -> Message:
        """Cancel a job order.

        Args:
            job_order_id: job order id
            token: authentication token

        Returns:
            Message: success message

        Raises:
            UnknownJobOrderError: unknown job order id
            CancellationUnauthorizedError: cancellation not available for this job order
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_JOB_ORDERS, job_order_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return Message.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_ERROR:
                raise CancellationUnauthorizedError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while cancelling the job order : {0}'.format(error))

    def stat(self, job_order_id: int, token: str) -> job_models.JobOrderStat:
        """Retrieve stats of a jobOrder.

        Args:
            job_order_id: job order id
            token: authentication token

        Returns:
            JobOrderStat: job order statistics instance

        Raises:
            UnknownJobOrderError: unknown job order id
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}/{2}/stat'.format(self._api_url, ENDPOINT_JOB_ORDERS, job_order_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return job_models.JobOrderStat.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while retrieving stat for the job order : {0}'.format(error))

    def job(self, job_id: int, token: str) -> job_models.Job:
        """Retrieve a job.

        Args:
            job_id: job_id
            token: authentication token

        Returns:
            Job: the job instance

        Raises:
            UnknownJobError: if job id is unknown
            ForbiddenParentJobOrderError: If the parent job order belongs to another user
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_JOBS, job_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return job_models.Job.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobError(job_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenParentJobOrderError()
            raise JobardError('Error while retrieving the job : {0}'.format(error))

    def progress(
        self,
        job_order_id: int,
        token: str,
        delay_beetween_loops: int = 30,
    ) -> Iterator[job_models.JobOrderStat]:
        """Follow the progression of a job order.

        Args:
            job_order_id: job order id
            delay_beetween_loops: delay between two loops
            token: authentication token

        Yields:
            JobOrderStat: statistics of the job order
        """
        response: job_models.JobOrderStat = self.stat(job_order_id=job_order_id, token=token)
        yield response
        while response.state not in states_closed:
            time.sleep(delay_beetween_loops)
            response = self.stat(job_order_id=job_order_id, token=token)
            yield response

    async def async_progress(
        self,
        job_order_id: int,
        token: str,
        delay_beetween_loops: int = 30,
    ) -> AsyncIterable[job_models.JobOrderStat]:
        """Follow the progression of a job order.

        Args:
            job_order_id: job order id
            token: authentication token
            delay_beetween_loops: delay between two loops

        Yields:
            JobOrderStat: statistics of the job order
        """
        response: job_models.JobOrderStat = self.stat(job_order_id=job_order_id, token=token)
        yield response
        while response.state not in states_closed:
            await asyncio.sleep(delay_beetween_loops)
            response = self.stat(job_order_id=job_order_id, token=token)
            yield response

    def job_orders(
        self,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
        states: Optional[List[State]] = None,
        name: Optional[str] = None,
    ) -> job_models.JobOrderList:
        """List the job's arguments of a job order.

        Args:
            token: authentication token
            offset: pagination offset
            limit: pagination limit
            states: state list
            name: search by job order name (contains)

        Returns:
            JobArgList: list of jobs arguments

        Raises:
            JobardError: error while requesting jobard api
        """

        # construct request
        url = '{0}/{1}?'.format(
            self._api_url,
            ENDPOINT_JOB_ORDERS,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        if states:
            params.extend(_build_state_filter(states))

        if name:
            params.extend([('name', '{}'.format(name))])

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return job_models.JobOrderList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while processing the response : {0}'.format(error))

    def all_job_orders(
        self,
        token: str,
        states: Optional[List[State]] = None,
        name: Optional[str] = None,
        limit: int = 1000,
    ) -> Iterator[job_models.JobOrder]:
        """Iterate over jobs.

        Args:
            token: authentication token
            states: list of states to filter
            name: search by job order name (contains)
            limit: pagination limit

        Yields:
            Job: a job instance
        """
        response = self.job_orders(token=token, name=name, offset=0, limit=0, states=states)
        offset = 0
        while offset < response.count:
            response = self.job_orders(token=token, name=name, offset=offset, limit=1000, states=states)
            yield from response.records
            offset += limit

    def jobs(
        self,
        job_order_id: int,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
        states: Optional[List[State]] = None,
    ) -> job_models.JobList:
        """List the jobs of a jobOrder.

        Args:
            job_order_id: job order id
            offset: pagination offset
            limit: pagination limit
            states: state list
            token: authentication token

        Returns:
            JobList: list of jobs of the job order

        Raises:
            UnknownJobOrderError: unknown job order id
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """

        # construct request
        url = '{0}/{1}/{2}/jobs?'.format(
            self._api_url,
            ENDPOINT_JOB_ORDERS,
            job_order_id,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        if states:
            params.extend(_build_state_filter(states))

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return job_models.JobList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while retrieving job IDs for the job order : {0}'.format(error))

    def all_jobs(
        self,
        job_order_id: int,
        token: str,
        states: Optional[List[State]] = None,
        limit: int = 1000,
    ) -> Iterator[job_models.Job]:
        """Iterate over jobs.

        Args:
            job_order_id: job order id
            token: authentication token
            states: list of states to filter
            limit: pagination limit

        Yields:
            Job: a job instance
        """
        response = self.jobs(job_order_id=job_order_id, token=token, offset=0, limit=0, states=states)
        offset = 0
        while offset < response.count:
            response = self.jobs(job_order_id=job_order_id, token=token, offset=offset, limit=1000, states=states)
            yield from response.records
            offset += limit

    def job_arguments(
        self,
        job_order_id: int,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
        states: Optional[List[State]] = None,
    ) -> job_models.JobArgList:
        """List the job's arguments of a job order.

        Args:
            job_order_id: job order id
            token: authentication token
            offset: pagination offset
            limit: pagination limit
            states: state list

        Returns:
            JobArgList: list of jobs arguments

        Raises:
            UnknownJobOrderError: unknown job order id
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """
        # construct request
        url = '{0}/{1}/{2}/arguments?'.format(
            self._api_url,
            ENDPOINT_JOB_ORDERS,
            job_order_id,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        if states:
            params.extend(_build_state_filter(states))

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return job_models.JobArgList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while retrieving arguments for the job order : {0}'.format(error))

    def all_arguments(
        self,
        job_order_id: int,
        token: str,
        states: Optional[List[State]] = None,
        limit: int = 1000,
    ) -> Iterator[Arguments]:
        """Iterate over job arguments.

        Args:
            job_order_id: job order id
            token: authentication token
            states: list of states to filter
            limit: pagination limit

        Yields:
            Arguments: arguments of a job
        """
        response = self.job_arguments(job_order_id=job_order_id, token=token, offset=0, limit=0, states=states)
        offset = 0
        while offset < response.count:
            response = self.job_arguments(
                job_order_id=job_order_id, token=token, offset=offset, limit=1000, states=states,
            )
            yield from response.records
            offset += limit

    def job_arrays(
        self,
        job_order_id: int,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
        states: Optional[List[State]] = None,
    ) -> job_models.JobArrayList:
        """List the job's arguments of a job order.

        Args:
            job_order_id: job order id
            token: authentication token
            offset: pagination offset
            limit: pagination limit
            states: state list

        Returns:
            JobArgList: list of jobs arguments

        Raises:
            UnknownJobOrderError: unknown job order id
            ForbiddenJobOrderError: If the job order belongs to another user
            JobardError: error while requesting jobard api
        """

        # construct request
        url = '{0}/{1}/{2}/arrays?'.format(
            self._api_url,
            ENDPOINT_JOB_ORDERS,
            job_order_id,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        if states:
            params.extend(_build_state_filter(states))

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return job_models.JobArrayList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownJobOrderError(job_order_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenJobOrderError(job_order_id)
            raise JobardError('Error while retrieving arguments for the job order : {0}'.format(error))

    def all_arrays(
        self,
        job_order_id: int,
        token: str,
        states: Optional[List[State]] = None,
        limit: int = 1000,
    ) -> Iterator[Arguments]:
        """Iterate over job arguments.

        Args:
            job_order_id: job order id
            token: authentication token
            states: list of states to filter
            limit: pagination limit

        Yields:
            Arguments: arguments of a job
        """
        response = self.job_arrays(job_order_id=job_order_id, token=token, offset=0, limit=0, states=states)
        offset = 0
        while offset < response.count:
            response = self.job_arrays(job_order_id=job_order_id, token=token, offset=offset, limit=1000, states=states)
            yield from response.records
            offset += limit

    def docker_images(
        self,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
    ) -> docker_models.DockerImageList:
        """List the Docker images of a user.

        Args:
            token: authentication token
            offset: pagination offset
            limit: pagination limit

        Returns:
            DockerImageList: list of user's Docker images

        Raises:
            JobardError: If error while retrieving the Docker images
        """
        # construct request
        url = '{0}/{1}?'.format(
            self._api_url,
            ENDPOINT_DOCKER_IMAGES,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return docker_models.DockerImageList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while processing the response : {0}'.format(error.response.text))

    def docker_image(self, image_id: int, token: str) -> docker_models.DockerImage:
        """Retrieve a Docker image.

        Args:
            image_id: Docker image id
            token: authentication token

        Returns:
            the Docker image instance

        Raises:
            UnknownDockerImageError: if image id is unknown
            ForbiddenDockerImageError: If the Docker image does not belong to the user
            JobardError: If error while retrieving the Docker image
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_DOCKER_IMAGES, image_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return docker_models.DockerImage.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerImageError(image_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenDockerImageError(docker_image_id=image_id)
            raise JobardError('Error while retrieving the Docker image : {0}'.format(error.response.text))

    def add_docker_image(self, docker_image: docker_models.DockerImageInput, token: str) -> Union[int, str]:
        """Create a Docker image.

        Args:
            docker_image: The Docker image to create
            token: authentication token

        Returns:
            The Docker image id

        Raises:
            AlreadyRegisteredDockerImageError: if a Docker image with the same name already exists for the user
            JobardError: If error while creating the Docker image
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_DOCKER_IMAGES),
            headers=_format_headers(token=token),
            data=docker_image.json(),
        )
        try:
            response.raise_for_status()
            try:
                return int(response.text)
            except ValueError:
                return response.text

        except requests.HTTPError as error:
            if response.status_code in STATUS_ERROR:
                raise AlreadyRegisteredDockerImageError(image_name=docker_image.name)
            raise JobardError('Error while creating the Docker image : {0}'.format(error.response.text))

    def update_docker_image(
        self,
        docker_image: docker_models.DockerImageUpdate,
        token: str,
    ) -> docker_models.DockerImage:
        """Update a Docker image.

        Args:
            docker_image: The Docker image to update
            token: authentication token

        Returns:
            The updated Docker image

        Raises:
            ForbiddenDockerImageError: If the Docker image does not belong to the user
            UnknownDockerImageError: if image id is unknown or disabled
            AlreadyRegisteredDockerImageError: if a Docker image with the same name already exists for the user
            JobardError: If error while updating the Docker image
        """
        response = requests.put(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_DOCKER_IMAGES),
            headers=_format_headers(token=token),
            data=docker_image.json(),
        )
        try:
            response.raise_for_status()
            return docker_models.DockerImage.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenDockerImageError(docker_image.image_id)
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerImageError(image_id=docker_image.image_id)
            if response.status_code in STATUS_ERROR:
                raise AlreadyRegisteredDockerImageError(image_name=docker_image.name)
            raise JobardError('Error while updating the Docker image : {0}'.format(error.response.text))

    def delete_docker_image(self, image_id: int, token: str) -> str:
        """Delete a Docker image.

        Args:
            image_id: Docker image id
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            UnknownDockerImageError: if image id is unknown or disabled
            ForbiddenDockerImageError: If the Docker image does not belong to the user
            JobardError: error while requesting jobard api
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_DOCKER_IMAGES, image_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'Docker image id {0} successfully deleted'.format(image_id)
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerImageError(image_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenDockerImageError(image_id)
            raise JobardError('Error while deleting the Docker image : {0}'.format(error.response.text))

    def docker_mount_points(
        self,
        image_id: int,
        token: str,
        offset: Optional[int] = 0,
        limit: Optional[int] = 1000,
    ) -> docker_models.DockerMountPointList:
        """List the Docker mount points of a Docker image.

        Args:
            image_id: Docker image id
            token: authentication token
            offset: pagination offset
            limit: pagination limit

        Returns:
            DockerMountPointList: list of image's Docker mount points

        Raises:
            ForbiddenDockerImageError: If the Docker image does not belong to the user
            JobardError: If error while retrieving the Docker mount points
        """
        # construct request
        url = '{0}/{1}/{2}?'.format(
            self._api_url,
            ENDPOINT_DOCKER_MOUNT_POINTS,
            image_id,
        )

        params = [
            ('offset', offset),
            ('limit', limit),
        ]

        # execute request
        response = requests.get(
            url=url + urllib.parse.urlencode(params),
            headers=_format_headers(token=token),
        )

        try:
            response.raise_for_status()
            return docker_models.DockerMountPointList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerImageError(image_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenDockerImageError(image_id)
            raise JobardError('Error while processing the response : {0}'.format(error.response.text))

    def docker_mount_point(self, mount_point_id: int, token: str) -> docker_models.DockerMountPoint:
        """Retrieve a Docker mount point.

        Args:
            mount_point_id: Docker mount point id
            token: authentication token

        Returns:
            the Docker mount point instance

        Raises:
            UnknownDockerMountPointError: if mount point id is unknown or image id is unknown or disabled
            ForbiddenParentDockerImageError: If the mount point's parent Docker image does not belong to the user
            JobardError: If error while retrieving the Docker mount point
        """
        response = requests.get(
            url='{0}/{1}/{2}/details'.format(self._api_url, ENDPOINT_DOCKER_MOUNT_POINTS, mount_point_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return docker_models.DockerMountPoint.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerMountPointError(mount_point_id=mount_point_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenParentDockerImageError()
            raise JobardError('Error while retrieving the Docker mount point : {0}'.format(error.response.text))

    def add_docker_mount_point(self, docker_mount_point: docker_models.DockerMountPointInput, token: str) -> int:
        """Create a Docker mount point.

        Args:
            docker_mount_point: The Docker mount point to create
            token: authentication token

        Returns:
            The Docker mount point id

        Raises:
            JobardError: If error while creating the Docker mount point
            ForbiddenDockerImageError: If the Docker image does not belong to the user
            UnknownDockerImageError: if image id is unknown or disabled
            AlreadyRegisteredDockerMountPointError: If a mount point with the same name already exists for the user
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_DOCKER_MOUNT_POINTS),
            headers=_format_headers(token=token),
            data=docker_mount_point.json(),
        )
        try:
            response.raise_for_status()
            return int(response.text)
        except requests.HTTPError as error:
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenDockerImageError(docker_mount_point.image_id)
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerImageError(docker_mount_point.image_id)
            if response.status_code in STATUS_ERROR:
                raise AlreadyRegisteredDockerMountPointError(docker_mount_point.name)
            raise JobardError('Error while creating the Docker mount point : {0}'.format(error.response.text))

    def update_docker_mount_point(
        self,
        docker_mount_point: docker_models.DockerMountPointUpdate,
        token: str,
    ) -> docker_models.DockerMountPoint:
        """Update a Docker mount point.

        Args:
            docker_mount_point: The Docker mount point to update
            token: authentication token

        Returns:
            The updated Docker mount point

        Raises:
            JobardError: If error while updating the Docker mount point
            ForbiddenParentDockerImageError: If the mount point's parent Docker image does not belong to the user
            UnknownDockerMountPointError: if mount point id is unknown or image id is unknown or disabled
            AlreadyRegisteredDockerMountPointError: If a mount point with the same name already exists for the user
        """
        response = requests.put(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_DOCKER_MOUNT_POINTS),
            headers=_format_headers(token=token),
            data=docker_mount_point.json(),
        )
        try:
            response.raise_for_status()
            return docker_models.DockerMountPoint.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenParentDockerImageError()
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerMountPointError(mount_point_id=docker_mount_point.mount_point_id)
            if response.status_code in STATUS_ERROR:
                raise AlreadyRegisteredDockerMountPointError(docker_mount_point.name)
            raise JobardError('Error while updating the Docker mount point : {0}'.format(error.response.text))

    def delete_docker_mount_point(self, mount_point_id: int, token: str) -> str:
        """Delete a Docker mount point.

        Args:
            mount_point_id: Docker mount point id
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            UnknownDockerMountPointError: if mount point id is unknown or image id is unknown or disabled
            ForbiddenParentDockerImageError: If the mount point's parent Docker image does not belong to the user
            JobardError: If error while deleting the Docker mount point
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_DOCKER_MOUNT_POINTS, mount_point_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'Docker mount point id {0} successfully deleted'.format(mount_point_id)
        except requests.HTTPError as error:
            if response.status_code in STATUS_UNKNOWN:
                raise UnknownDockerMountPointError(mount_point_id=mount_point_id)
            if response.status_code in STATUS_FORBIDDEN:
                raise ForbiddenParentDockerImageError()
            raise JobardError('Error while deleting the Docker mount point : {0}'.format(error.response.text))

    def health(self) -> monitoring_models.MonitoringResult:
        """Retrieve monitoring results (jobard api, database and daemons health per host connection).

        Returns:
            the monitoring information.

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_HEALTH),
        )
        try:
            response.raise_for_status()
            return monitoring_models.MonitoringResult.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving health information : {0}'.format(error))

    def connections(
        self
    ) -> monitoring_models.RemoteConnectionList:
        """List the hosts connections of the daemons.

        Returns:
            ConnectionList: list of connections

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_CONNECTIONS),
        )
        try:
            response.raise_for_status()
            return monitoring_models.RemoteConnectionList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving host connections information : {0}'.format(error))

    def add_connection(self, connection: monitoring_models.RemoteConnectionInput, token: str) -> str:
        """Create a remote connection.

        Args:
            connection: The remote connection to create
            token: authentication token

        Raises:
            JobardError: If error while creating the remote connection
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_CONNECTIONS),
            headers=_format_headers(token=token),
            data=connection.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the remote connection : {0}'.format(error.response.text))

    def update_connection(
            self,
            connection: monitoring_models.RemoteConnectionUpdate,
            token: str,
        ) -> monitoring_models.RemoteConnection:
            """Update a remote connection.

            Args:
                connection: The remote connection to update
                token: authentication token

            Returns:
                The updated remote connection

            Raises:
                JobardError: If error while updating the remote connection
            """
            response = requests.put(
                url='{0}/{1}/'.format(self._api_url, ENDPOINT_CONNECTIONS),
                headers=_format_headers(token=token),
                data=connection.json(),
            )
            try:
                response.raise_for_status()
                return monitoring_models.RemoteConnection.parse_obj(json.loads(response.text))
            except requests.HTTPError as error:
                raise JobardError('Error while updating the remote connection : {0}'.format(error.response.text))

    def delete_connection(self, name: str, daemon_id: str, token: str) -> str:
        """Delete a remote connection.

        Args:
            name: remote connection name
            daemon_id: remote connection daemon ID
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            JobardError: error while delete the remote connection
        """

        response = requests.delete(
            url='{0}/{1}/{2}/{3}'.format(self._api_url, ENDPOINT_CONNECTIONS, name, daemon_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'remote connection name : {0} for daemon : {1} successfully deleted'.format(name, daemon_id)
        except requests.HTTPError as error:
            raise JobardError('Error while deleting the remote connection {0} '.format(error.response.text))

    def connection(self, name: str, daemon_id: str, token: str) -> monitoring_models.RemoteConnection:
        """Retrieve a remote connection.

        Args:
            name: The connection name
            daemon_id: The connection daemon id
            token: authentication token

        Returns:
            the remote connection instance

        Raises:
            JobardError: If error while retrieving the connection
        """
        response = requests.get(
            url='{0}/{1}/{2}/{3}'.format(self._api_url, ENDPOINT_CONNECTIONS, name, daemon_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return monitoring_models.RemoteConnection.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving the remote connection : {0}'.format(error.response.text))

    def clusters(self) -> monitoring_models.JobardClusterList:
        """List the clusters stored in the database.

        Returns:
            JobardClusterList: The clusters' list

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_CLUSTERS),
        )
        try:
            response.raise_for_status()
            return monitoring_models.JobardClusterList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving clusters information : {0}'.format(error))

    def add_cluster(self, cluster: monitoring_models.JobardClusterInput, token: str) -> str:
        """Create a Jobard cluster.

        Args:
            cluster: The cluster to create
            token: authentication token

        Raises:
            JobardError: If error while creating the cluster
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_CLUSTERS),
            headers=_format_headers(token=token),
            data=cluster.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the remote cluster : {0}'.format(error.response.text))

    def update_cluster(
            self,
            cluster: monitoring_models.JobardClusterUpdate,
            token: str,
        ) -> monitoring_models.JobardCluster:
            """Update a Jobard cluster.

            Args:
                cluster: The cluster to update
                token: authentication token

            Returns:
                The updated cluster

            Raises:
                JobardError: If error while updating the cluster
            """
            response = requests.put(
                url='{0}/{1}/'.format(self._api_url, ENDPOINT_CLUSTERS),
                headers=_format_headers(token=token),
                data=cluster.json(),
            )
            try:
                response.raise_for_status()
                return monitoring_models.JobardCluster.parse_obj(json.loads(response.text))
            except requests.HTTPError as error:
                raise JobardError('Error while updating the cluster : {0}'.format(error.response.text))

    def delete_cluster(self, cluster_id: int, token: str) -> str:
        """Delete a Jobard cluster.

        Args:
            cluster_id: cluster ID
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            JobardError: error while delete the cluster
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_CLUSTERS, cluster_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'Jobard cluster ID : {0} successfully deleted'.format(cluster_id)
        except requests.HTTPError as error:
            raise JobardError('Error while deleting the Jobard cluster {0} '.format(error.response.text))

    def cluster(self, cluster_id: int, token: str) -> monitoring_models.JobardCluster:
        """Retrieve a Jobard cluster.

        Args:
            cluster_id: The cluster ID
            token: authentication token

        Returns:
            the cluster instance

        Raises:
            JobardError: If error while retrieving the cluster
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_CLUSTERS, cluster_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return monitoring_models.JobardCluster.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving the Jobard cluster : {0}'.format(error.response.text))

    def users(self, token: str) -> users_models.JobardUserList:
        """List the users stored in the database.

        Args:
            token: authentication token

        Returns:
            JobardUserList: The users' list

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_USERS),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.JobardUserList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving users information : {0}'.format(error))

    def add_user(self, user: users_models.JobardUserInput, token: str) -> str:
        """Create a Jobard user.

        Args:
            user: The user to create
            token: authentication token

        Raises:
            JobardError: If error while creating the user
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_USERS),
            headers=_format_headers(token=token),
            data=user.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the Jobard user : {0}'.format(error.response.text))

    def update_user(
            self,
            user: users_models.JobardUserUpdate,
            token: str,
        ) -> users_models.JobardUser:
            """Update a Jobard user.

            Args:
                user: The user to update
                token: authentication token

            Returns:
                The updated user

            Raises:
                JobardError: If error while updating the user
            """
            response = requests.put(
                url='{0}/{1}/'.format(self._api_url, ENDPOINT_USERS),
                headers=_format_headers(token=token),
                data=user.json(),
            )
            try:
                response.raise_for_status()
                return users_models.JobardUser.parse_obj(json.loads(response.text))
            except requests.HTTPError as error:
                raise JobardError('Error while updating the Jobard user : {0}'.format(error.response.text))

    def delete_user(self, user_id: int, token: str) -> str:
        """Delete a Jobard user.

        Args:
            user_id: user ID
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            JobardError: error while delete the user
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_USERS, user_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'Jobard user ID : {0} successfully deleted'.format(user_id)
        except requests.HTTPError as error:
            raise JobardError('Error while deleting the Jobard user {0} '.format(error.response.text))

    def user(self, user_id: int, token: str) -> users_models.JobardUser:
        """Retrieve a Jobard user.

        Args:
            user_id: The user ID
            token: authentication token

        Returns:
            the user instance

        Raises:
            JobardError: If error while retrieving the user
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_USERS, user_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.JobardUser.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving the Jobard user : {0}'.format(error.response.text))

    def cluster_accesses(
        self,
        token: str,
    ) -> users_models.ClusterAccessList:
        """List the cluster accesses.

        Args:
            token: authentication token

        Returns:
            ClusterAccessList: list of cluster accesses

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_CLUSTER_ACCESSES),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.ClusterAccessList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving cluster accesses information : {0}'.format(error))

    def add_cluster_access(self, cluster_access: users_models.ClusterAccessInput, token: str) -> str:
        """Create a cluster access.

        Args:
            cluster_access: The cluster access to create
            token: authentication token

        Raises:
            JobardError: If error while creating the cluster access
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_CLUSTER_ACCESSES),
            headers=_format_headers(token=token),
            data=cluster_access.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the cluster access : {0}'.format(error.response.text))

    def update_cluster_access(
            self,
            cluster_access: users_models.ClusterAccessUpdate,
            token: str,
        ) -> users_models.ClusterAccess:
            """Update a cluster access.

            Args:
                cluster_access: The cluster access to update
                token: authentication token

            Returns:
                The updated cluster access

            Raises:
                JobardError: If error while updating the cluster access
            """
            response = requests.put(
                url='{0}/{1}/'.format(self._api_url, ENDPOINT_CLUSTER_ACCESSES),
                headers=_format_headers(token=token),
                data=cluster_access.json(),
            )
            try:
                response.raise_for_status()
                return users_models.ClusterAccess.parse_obj(json.loads(response.text))
            except requests.HTTPError as error:
                raise JobardError('Error while updating the cluster access : {0}'.format(error.response.text))

    def delete_cluster_access(self, cluster_access_id: int, token: str) -> str:
        """Delete a cluster access.

        Args:
            cluster_access_id: cluster access ID
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            JobardError: error while delete the cluster access
        """

        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_CLUSTER_ACCESSES, cluster_access_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'remote cluster access : {0} successfully deleted'.format(cluster_access_id)
        except requests.HTTPError as error:
            raise JobardError('Error while deleting the cluster access {0} '.format(error.response.text))

    def cluster_access(self, cluster_access_id: int, token: str) -> users_models.ClusterAccess:
        """Retrieve a cluster access.

        Args:
            cluster_access_id: The cluster access id
            token: authentication token

        Returns:
            the cluster access instance

        Raises:
            JobardError: If error while retrieving the cluster access
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_CLUSTER_ACCESSES, cluster_access_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.ClusterAccess.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving the cluster access : {0}'.format(error.response.text))

    def access_tokens(self, token: str) -> users_models.AccessTokenList:
        """List the access tokens stored in the database.

        Args:
            token: authentication token

        Returns:
            AccessTokenList: The access tokens' list

        Raises:
            JobardError: error while requesting jobard api
        """
        response = requests.get(
            url='{0}/{1}'.format(self._api_url, ENDPOINT_ACCESS_TOKENS),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.AccessTokenList.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving access tokens information : {0}'.format(error))

    def add_access_token(self, access_token: users_models.AccessTokenInput, token: str) -> str:
        """Create an access token.

        Args:
            access_token: The access token to create
            token: authentication token

        Raises:
            JobardError: If error while creating the access token
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_ACCESS_TOKENS),
            headers=_format_headers(token=token),
            data=access_token.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the access token : {0}'.format(error.response.text))

    def update_access_token(
            self,
            access_token: users_models.AccessTokenUpdate,
            token: str,
        ) -> users_models.AccessToken:
            """Update an access token.

            Args:
                access_token: The access token to update
                token: authentication token

            Returns:
                The updated access token

            Raises:
                JobardError: If error while updating the access token
            """
            response = requests.put(
                url='{0}/{1}/'.format(self._api_url, ENDPOINT_ACCESS_TOKENS),
                headers=_format_headers(token=token),
                data=access_token.json(),
            )
            try:
                response.raise_for_status()
                return users_models.AccessToken.parse_obj(json.loads(response.text))
            except requests.HTTPError as error:
                raise JobardError('Error while updating the access token : {0}'.format(error.response.text))

    def delete_access_token(self, access_token_id: int, token: str) -> str:
        """Delete an access token.

        Args:
            access_token_id: access token ID
            token: authentication token

        Returns:
            A confirmation message

        Raises:
            JobardError: error while delete the access token
        """
        response = requests.delete(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_ACCESS_TOKENS, access_token_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            if response.status_code in STATUS_OK:
                return 'Access token ID : {0} successfully deleted'.format(access_token_id)
        except requests.HTTPError as error:
            raise JobardError('Error while deleting the access token {0} '.format(error.response.text))

    def access_token(self, access_token_id: int, token: str) -> users_models.AccessToken:
        """Retrieve an access token.

        Args:
            access_token_id: The access token ID
            token: authentication token

        Returns:
            the access token instance

        Raises:
            JobardError: If error while retrieving the access token
        """
        response = requests.get(
            url='{0}/{1}/{2}'.format(self._api_url, ENDPOINT_ACCESS_TOKENS, access_token_id),
            headers=_format_headers(token=token),
        )
        try:
            response.raise_for_status()
            return users_models.AccessToken.parse_obj(json.loads(response.text))
        except requests.HTTPError as error:
            raise JobardError('Error while retrieving the access token : {0}'.format(error.response.text))

    def add_connection_cluster(self, connection_cluster: monitoring_models.RemoteConnectionClusterInput, token: str) -> str:
        """Create a remote connection to a cluster.

        Args:
            connection_cluster: The remote connection and its cluster to create
            token: authentication token

        Raises:
            JobardError: If error while creating the remote connection and its cluster
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_CONNECTION_CLUSTER),
            headers=_format_headers(token=token),
            data=connection_cluster.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError('Error while creating the remote connection and its cluster : {0}'.format(error.response.text))

    def add_user_access(self, user_access: users_models.UserAccessInput,
                               token: str) -> str:
        """Create a Jobard user and its token.

        Args:
            user_access: The Jobard user and its token to create
            token: authentication token

        Raises:
            JobardError: If error while creating the Jobard user and its token
        """
        # execute request
        response = requests.post(
            url='{0}/{1}/'.format(self._api_url, ENDPOINT_USER_ACCESS),
            headers=_format_headers(token=token),
            data=user_access.json(),
        )
        try:
            response.raise_for_status()
            return response.text
        except requests.HTTPError as error:
            raise JobardError(
                'Error while creating the Jobard user and its token : {0}'.format(error.response.text))