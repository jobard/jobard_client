"""Command line interface utilities."""
import logging
import os

from jobard_client.core.core_types import State
from jobard_client.core.jobs.models import JobOrderStat


def format_state(state: JobOrderStat) -> str:
    """Format job order stat for progressbar.

    Args:
        state: job order stat

    Returns:
        formatted state
    """
    if not state:
        return 'Not yet running !'

    nb_succeeded = 0
    nb_failed = 0
    if State.SUCCEEDED in state.progress.job_states:
        nb_succeeded = state.progress.job_states.get(State.SUCCEEDED)

    if State.FAILED in state.progress.job_states:
        nb_failed = state.progress.job_states.get(State.FAILED)

    nb_remaining = state.job_count - nb_succeeded - nb_failed

    return 'R:{0} | S:{1} | F:{2} '.format(nb_remaining, nb_succeeded, nb_failed)


def get_logging_level(verbose: int = 0) -> int:
    """Retrieve logging level from int from 0 to 3.

    Args:
        verbose: verbosity level

    Returns:
        int: logging level
    """
    if verbose == 0:
        return logging.ERROR
    if verbose == 1:
        return logging.WARNING
    if verbose == 2:
        return logging.INFO
    return logging.DEBUG


def handle_token(cli_token: str) -> str:
    """Handle token either from environment variable or command option, the latter being priority.

    Args:
        cli_token: The client token

    Returns:
        str: Either client or environment variable token
    """
    token = os.getenv('JOBARD_TOKEN')
    if cli_token:
        token = cli_token
    return token
