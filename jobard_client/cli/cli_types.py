"""CLI types module."""
import re
import click

from jobard_client.core.core_types import Format, State, Cluster, \
    MemoryString, WalltimeString, SplitString, MEMORY_PATTERN, WALLTIME_PATTERN, SPLIT_PATTERN, CommandAlias, \
    COMMAND_ALIAS_PATTERN, JobOrderName

COLOR_RED = 'red'


class StateType(click.ParamType):
    """State type."""

    name = 'state'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A State
        """
        try:
            return State(value)
        except ValueError:
            self.fail('{0} is not a valid state'.format(value))


class EnvExtraType(click.ParamType):
    """Environnement extra variable type."""

    name = 'env_extra'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A tuple of two strings.
        """
        try:
            env_extra = tuple(value.split(':'))
        except ValueError:
            self.fail(click.style('{0} is not a valid env extra, must be formatted as : "<variable>:<value>"'
                                  .format(value),fg=COLOR_RED))

        if len(env_extra) != 2:
            self.fail(click.style('{0} is not a valid env extra, must be formatted as : "<variable>:<value>"'
                                  .format(value),fg=COLOR_RED))
        return env_extra


class SplitType(click.ParamType):
    """Split type."""

    name = 'split'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A Split
        """
        try:
            split = SplitString(value)
            if not re.match(SPLIT_PATTERN, split):
                self.fail(click.style('{0} is not a valid split'.format(value), fg=COLOR_RED))
            return split
        except ValueError:
            self.fail(click.style('{0} is not a valid split'.format(value), fg=COLOR_RED))


class WalltimeType(click.ParamType):
    """Walltime type."""

    name = 'walltime'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A Walltime
        """
        try:
            walltime = WalltimeString(value)
            if not re.match(WALLTIME_PATTERN, walltime):
                self.fail(click.style('{0} is not a valid walltime'.format(value), fg=COLOR_RED))
            return walltime
        except ValueError:
            self.fail(click.style('{0} is not a valid walltime'.format(value), fg=COLOR_RED))


class MemoryType(click.ParamType):
    """Memory type."""

    name = 'memory'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A Memory
        """
        try:
            memory = MemoryString(value)
            if not re.match(MEMORY_PATTERN, memory):
                self.fail(click.style('{0} is not a valid memory'.format(value), fg=COLOR_RED))
            return memory
        except ValueError:
            self.fail(click.style('{0} is not a valid memory'.format(value), fg=COLOR_RED))


class FormatType(click.ParamType):
    """Format type."""

    name = 'format'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A Format
        """
        try:
            return Format(value)
        except ValueError:
            self.fail('{0} is not a valid format'.format(value))

class ClusterType(click.ParamType):
    """Jobard cluster type."""

    name = 'cluster_type'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A Cluster
        """
        try:
            return Cluster(value)
        except ValueError:
            self.fail('{0} is not a valid format'.format(value))

class FileType(click.Path):
    """File type."""

    name = 'file'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A String with the file content
        """

        content = None
        if value:
            try:
                # Read the SSH key from the path
                with open(value) as f:
                    content = f.read().strip()
            except OSError:
                self.fail(click.style('{0} reading failed'.format(value), fg=COLOR_RED))

        return content


class CommandAliasType(click.ParamType):
    """Command alias type."""

    name = 'command_alias'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A CommandAlias
        """
        try:
            cmd_alias = CommandAlias(value)
            # Check the pattern
            if not re.match(COMMAND_ALIAS_PATTERN, cmd_alias):
                self.fail(click.style('{0} is not a valid command alias'.format(value), fg=COLOR_RED))
            # Check the length
            if len(cmd_alias) < 1 or len(cmd_alias) > 32:
                self.fail(click.style(
                    'command_alias length must be at least 1 character and be lower than 32, got : {0}'.
                    format(len(cmd_alias)), fg=COLOR_RED))
            return cmd_alias
        except ValueError:
            self.fail(click.style('{0} is not a valid command alias'.format(value), fg=COLOR_RED))

class JobOrderNameType(click.ParamType):
    """Job order name type."""

    name = 'job_order_name'

    def convert(self, value: str, param=None, ctx=None):  # noqa: WPS110
        """Convert the value to the correct type.

        Args:
            value: The value to convert.
            param: The parameter that is using this type to convert its value.
            ctx: The current context that arrived at this value.

        Returns:
            A JobOrderName
        """
        try:
            job_order_name = JobOrderName(value)
            # Check the pattern
            if not re.match(COMMAND_ALIAS_PATTERN, job_order_name):
                self.fail(click.style('{0} is not a valid job order name'.format(value), fg=COLOR_RED))
            return job_order_name
        except ValueError:
            self.fail(click.style('{0} is not a valid job order'.format(value), fg=COLOR_RED))

job_order_id_argument = click.argument('job_order_id', type=int)

api_option = click.option(
    '--api',
    type=str,
    default='http://localhost:8000',
    envvar='JOBARD_API_URL',
    help='Jobard api URL',
)
verbose_option = click.option(
    '-v',
    '--verbose',
    count=True,
    default=0,
    help='Verbosity of messages: "-v" for normal output, "-vv" for more verbose output and "-vvv" for debug',
)

offset_option = click.option(
    '--offset',
    type=int,
    default=0,
    help='Query offset',
)

limit_option = click.option(
    '--limit',
    type=int,
    default=1000,
    help='Query limit',
)

states_option = click.option(
    '--state',
    'states',
    multiple=True,
    type=StateType(),
    default=[],
    help='Job state filter, example: --state RUNNING --state NEW',
)

format_option = click.option(
    '--output_format',
    'output_format',
    type=FormatType(),
    default=Format.TABLE,
    help='Output format, example: --output_format CSV --output_format JSON --output_format TABLE',
)

token_option = click.option(
    '--token',
    '-T',
    type=str,
    help='Authentification token'
)

unix_uid_option = click.option(
    '--unix_uid',
    type=int,
    help='The Unix UID of the user.',
)

password_option = click.option(
    '--password',
    type=str,
    help='The remote user password.',
)

ssh_key_file_option = click.option(
    '--ssh_key_file',
    type=FileType(exists=True),
    default=None,
    help='The path to remote user SSH private key file.',
)

healthcheck_interval_option = click.option(
    '--healthcheck_interval',
    type=str,
    default='24:00:00',
    help='Healthcheck cluster period (ISO 8601 format for timedelta : [±]P[DD]DT[HH]H[MM]M[SS]S, default: 24:00:00. Example --healthcheck_interval P2DT0H0M0S for 2 days)',
)

remote_username_option = click.option(
    '--remote_username',
    type=str,
    help='The remote user username.',
)

cluster_type_option = click.option(
    '--type',
    'cluster_type',
    type=ClusterType(),
    default=Cluster.LOCAL,
    help='Cluster type value between : LOCAL, SWARM, PBS, HTCONDOR and KUBE. Example: --cluster_type LOCAL',
)

docker_image_id_argument = click.argument('docker_image_id', type=int)

docker_mount_point_id_argument = click.argument('docker_mount_point_id', type=int)

connection_daemon_id_argument = click.argument('connection_daemon_id', type=str, default="0")

connection_name_argument = click.argument('connection_name', type=str)

cluster_id_argument = click.argument('cluster_id', type=int)

user_id_argument = click.argument('user_id', type=int)

cluster_access_name_argument = click.argument('cluster_access_name', type=str)

cluster_access_id_argument = click.argument('cluster_access_id', type=int)

access_token_id_argument = click.argument('access_token_id', type=int)