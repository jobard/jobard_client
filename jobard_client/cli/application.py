"""Jobard client console application."""
import logging
import shlex
import sys
import time
from datetime import timedelta
from pathlib import Path
from typing import List, Tuple

import click

from jobard_client.cli.cli_types import (
    COLOR_RED,
    MemoryType,
    SplitType,
    WalltimeType,
    format_option,
    job_order_id_argument,
    limit_option,
    offset_option,
    states_option,
    token_option,
    docker_image_id_argument,
    docker_mount_point_id_argument, connection_name_argument, connection_daemon_id_argument,
    cluster_id_argument, user_id_argument, cluster_access_name_argument, cluster_access_id_argument,
    access_token_id_argument, password_option, ssh_key_file_option, unix_uid_option, healthcheck_interval_option,
    remote_username_option, cluster_type_option, EnvExtraType, CommandAliasType, JobOrderNameType,
)
from jobard_client.cli.helpers import format_state, get_logging_level
from jobard_client.client import JobardClient
from jobard_client.core.core_types import Format, State, states_closed, Cluster, MemoryString, WalltimeString, \
    SplitString, CommandAlias, JobOrderName
from jobard_client.core.docker.models import (
    DockerImageInput,
    DockerMountPointInput,
    DockerImageUpdate,
    DockerMountPointUpdate,
    DockerVisitorFactory,
)
from jobard_client.core.exceptions import JobardError
from jobard_client.core.jobs.models import JobOrderSubmit, JobVisitorFactory
from jobard_client.core.monitoring.models import MonitoringVisitorFactory, RemoteConnectionInput, \
    RemoteConnectionUpdate, JobardClusterInput, JobardClusterUpdate, RemoteConnectionClusterInput
from jobard_client.core.users.models import JobardUserInput, UserVisitorFactory, \
    JobardUserUpdate, ClusterAccessInput, ClusterAccessUpdate, AccessTokenInput, \
    AccessTokenUpdate, UserAccessInput
from jobard_client.exceptions import UnknownJobOrderError


@click.group()
@click.version_option()
@click.option(
    '--api',
    type=str,
    default='http://localhost:8000',
    envvar='JOBARD_API_URL',
    help='Jobard api URL',
)
@click.option(
    '-v',
    '--verbose',
    count=True,
    default=0,
    help='Verbosity of messages: "-v" for normal output, "-vv" for more verbose output and "-vvv" for debug',
)
@click.pass_context
def cli(ctx, api: str, verbose: int):
    """Jobard client console application.

    Args:
        ctx: client context object to store jobard client
        api: jobard api url
        verbose: verbosity level (0: ERROR ; 1: WARNING ; 2: INFO ; 3:  DEBUG)
    """
    logging.basicConfig(level=get_logging_level(verbose))
    ctx.ensure_object(JobardClient)
    ctx.obj = JobardClient(api)


@cli.command()
def about():
    """Show information about Jobard."""
    click.echo('Jobard client')


@cli.command()  # noqa: WPS211, WPS216
@click.argument('command',  type=str)
@click.pass_obj
@token_option
@click.option(
    '--name',
    type=JobOrderNameType(),
    help='Job order name. (default: <USER>_<DATETIME>)',
)
@click.option(
    '--arguments',
    '-a',
    type=str,
    multiple=True,
    default=[] if sys.stdin.isatty() else sys.stdin.readlines(),
    help='The arguments of the command',
)
@click.option(
    '--image_id',
    type=int,
    help='The Docker image ID. Mandatory for Swarm and Kubernetes clusters',
)
@click.option(
    '--interpolate_command',
    is_flag=True,
    help='Interpolate the command to execute',
)
@click.option(
    '--split',
    type=SplitType(),
    default=None,
    help='Strategy to split commands into array of jobs',
)
@click.option(
    '--priority',
    type=int,
    default=1,
    help='Job order priority (default: 1)',
)
@click.option(
    '--cores',
    type=int,
    default=1,
    help='Maximum amount of CPU cores to use (default: 1)',
)
@click.option(
    '--memory',
    type=MemoryType(),
    default='512M',
    help='Maximum amount of memory to use (unit : K|M|G ; default: 512M)',
)
@click.option(
    '--walltime',
    type=WalltimeType(),
    default='01:00:00',
    help='Maximum amount of CPU time to use (format: HH:MM:SS, default: 01:00:00)',
)
@click.option(
    '--connection',
    type=str,
    help='Execute command on a remote cluster via an ssh connection',
)
@click.option(
    '--job-extra',
    '-J',
    type=str,
    default=None,
    multiple=True,
    help='Extra job configuration (multiple values allowed)',
)
@click.option(
    '--env-extra',
    '-E',
    type=EnvExtraType(),
    default=None,
    multiple=True,
    help='Extra env configuration (multiple values allowed)',
)
@click.option(
    '--command_alias',
    type=CommandAliasType(),
    help='Nickname for the command. This will be used in the Job logs directories structure',
)
@click.option(
    '--job_log_prefix_arg_idx',
    type=int,
    help='Index in the Job Order arguments used for prefixing Job log files',
)
def submit(
    jobard_client: JobardClient,
    command: str,
    name: JobOrderName,
    arguments: Tuple[str],
    interpolate_command: bool,
    split: SplitString,
    priority: int,
    cores: int,
    memory: MemoryString,
    walltime: WalltimeString,
    connection: str,
    job_extra: List[str],
    env_extra: List[Tuple[str, str]],
    image_id: int,
    command_alias: CommandAlias,
    job_log_prefix_arg_idx: int,
    token: str,
):
    """Submit a job order.
    \f
    Args:
        jobard_client: jobard client
        command: The command to execute, enclosed with double quotes
        name: Job order name
        arguments: The arguments of the command
        interpolate_command: Interpolate the command to execute
        split: Strategy to split commands into array of jobs
        priority: Job order priority (default: 1)
        cores: Maximum amount of CPU cores to use (default: 1)
        memory: Maximum amount of memory to use (unit : K|M|G ; default: 512M)
        walltime: Maximum amount of CPU time to use (format: HH:MM:SS, default: 00:05:00)
        connection: Execute command on a remote cluster via an ssh connection
        job_extra: Extra job configuration (multiple values allowed)
        env_extra: Extra env configuration (multiple values allowed)
        image_id: Docker image id
        job_log_prefix_arg_idx:  Index in the Job Order arguments used for prefixing Job log files
        command_alias: Nickname for the command. This will be used in the Job logs directories structure
        token: authentication token
    """
    try:
        job_order_id = jobard_client.submit(
            joborder=JobOrderSubmit(
                name=name,
                command=shlex.split(command),
                arguments=[shlex.split(argument) for argument in arguments],
                interpolate_command=interpolate_command,
                split=split,
                priority=priority,
                cores=cores,
                memory=memory,
                walltime=walltime,
                connection=connection,
                job_extra=job_extra,
                env_extra=dict(env_extra),
                image_id=image_id,
                command_alias=command_alias,
                job_log_prefix_arg_idx=job_log_prefix_arg_idx,
            ),
            token=token,
        )
        click.echo(job_order_id)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@job_order_id_argument
def cancel(jobard_client: JobardClient, job_order_id: int, token: str):
    """Cancel a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        token: authentication token
    """
    try:
        click.echo(jobard_client.cancel(job_order_id=job_order_id, token=token).model_dump_json())
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@job_order_id_argument
@format_option
def order(
    jobard_client: JobardClient,
    job_order_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        job_order = jobard_client.job_order(job_order_id=job_order_id, token=token)
        job_order.accept(JobVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@job_order_id_argument
@format_option
def stat(
    jobard_client: JobardClient,
    job_order_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve statistics of a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        job_order_stat = jobard_client.stat(job_order_id=job_order_id, token=token)
        job_order_stat.accept(JobVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@job_order_id_argument
def progress(
    jobard_client: JobardClient,
    job_order_id: int,
    token: str,
):
    """Display the progression of a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: job order id
        token: authentication token
    """
    try:
        error_message = None
        # create progress bar
        with click.progressbar(
            label='Job order {0} ...'.format(job_order_id),
            fill_char=click.style('#', fg='green'),
            empty_char=click.style('-', fg='white', dim=True),
            show_eta=False,
            show_pos=False,
            length=100,
            width=100,
            item_show_func=format_state,
        ) as progress_bar:
            # loop over progression and update progress bar
            percentage = 0
            for jos in jobard_client.progress(job_order_id=job_order_id, delay_beetween_loops=1, token=token):
                new_percentage = int(jos.progress.percentage * 100)
                if jos.error_message is not None:
                    error_message = jos.error_message
                else:
                    progress_bar.update(n_steps=new_percentage - percentage, current_item=jos)
                percentage = new_percentage
        if error_message:
            click.echo(click.style('Job order {0} ...  ERROR: {1}'.format(job_order_id, error_message), fg=COLOR_RED))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@job_order_id_argument
@offset_option
@limit_option
@states_option
@format_option
def jobs(
    jobard_client: JobardClient,
    job_order_id: int,
    offset: int,
    limit: int,
    states: List[State],
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the jobs of a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        offset: pagination offset
        limit: pagination limit
        states: state list
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        job_order_list = jobard_client.jobs(
            job_order_id=job_order_id,
            offset=offset,
            limit=limit,
            states=states,
            token=token,
        )
        job_order_list.accept(JobVisitorFactory.create_visitor(output_format))

        if job_order_list.count > (job_order_list.offset + job_order_list.limit):
            click.echo('*' * 70)
            click.echo('Nb jobs : {0} ; Offset : {1} ; Limit : {2}'.format(
                job_order_list.count,
                job_order_list.offset,
                job_order_list.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command('args')  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@job_order_id_argument
@offset_option
@limit_option
@states_option
@format_option
def job_args(
    jobard_client: JobardClient,
    job_order_id: int,
    offset: int,
    limit: int,
    states: List[State],
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the arguments of a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        offset: pagination offset
        limit: pagination limit
        states: state list
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_arguments = jobard_client.job_arguments(
            job_order_id=job_order_id,
            offset=offset,
            limit=limit,
            states=states,
            token=token,
        )

        list_arguments.accept(JobVisitorFactory.create_visitor(output_format))

        if list_arguments.count > (list_arguments.offset + list_arguments.limit):
            click.echo('*' * 70)
            click.echo('Nb jobs : {0} ; Offset : {1} ; Limit : {2}'.format(
                list_arguments.count,
                list_arguments.offset,
                list_arguments.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.argument('job_id', type=int)
@click.pass_obj
@token_option
@format_option
def job(
    jobard_client: JobardClient,
    job_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a report of a job.
    \f
    Args:
        jobard_client: jobard client
        job_id: job ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        job_obj = jobard_client.job(job_id=job_id, token=token)
        job_obj.accept(JobVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@job_order_id_argument
@offset_option
@limit_option
@states_option
@format_option
def arrays(
    jobard_client: JobardClient,
    job_order_id: int,
    offset: int,
    limit: int,
    states: List[State],
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the job arrays of a job order.
    \f
    Args:
        jobard_client: jobard client
        job_order_id: jobOrder ID
        offset: pagination offset
        limit: pagination limit
        states: state list
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_arrays = jobard_client.job_arrays(
            job_order_id=job_order_id,
            offset=offset,
            limit=limit,
            states=states,
            token=token,
        )
        list_arrays.accept(JobVisitorFactory.create_visitor(output_format))

        if list_arrays.count > (list_arrays.offset + list_arrays.limit):
            click.echo('*' * 70)
            click.echo('Nb arrays : {0} ; Offset : {1} ; Limit : {2}'.format(
                list_arrays.count,
                list_arrays.offset,
                list_arrays.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@offset_option
@limit_option
@states_option
@format_option
@click.option(
    '--name',
    type=str,
    help='Job order name (contains)',
)
def orders(
    jobard_client: JobardClient,
    offset: int,
    limit: int,
    states: List[State],
    name: str,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the job arrays of a job order.
    \f
    Args:
        jobard_client: jobard client
        offset: pagination offset
        limit: pagination limit
        states: state list
        name: job order name (contains)
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_jobs = jobard_client.job_orders(
            name=name,
            offset=offset,
            limit=limit,
            states=states,
            token=token,
        )

        if not list_jobs.count:
            sys.exit(0)

        list_jobs.accept(JobVisitorFactory.create_visitor(output_format))

        if list_jobs.count > (list_jobs.offset + list_jobs.limit):
            click.echo('*' * 70)
            click.echo('Nb job orders : {0} ; Offset : {1} ; Limit : {2}'.format(
                list_jobs.count,
                list_jobs.offset,
                list_jobs.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@offset_option
@limit_option
@format_option
def docker_images(
    jobard_client: JobardClient,
    offset: int,
    limit: int,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the Docker images of a user.
    \f
    Args:
        jobard_client: jobard client
        offset: pagination offset
        limit: pagination limit
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_docker_images = jobard_client.docker_images(
            token=token,
            offset=offset,
            limit=limit,
        )

        if not list_docker_images.count:
            sys.exit(0)

        list_docker_images.accept(DockerVisitorFactory.create_visitor(output_format))

        if list_docker_images.count > (list_docker_images.offset + list_docker_images.limit):
            click.echo('*' * 70)
            click.echo('Nb docker images : {0} ; Offset : {1} ; Limit : {2}'.format(
                list_docker_images.count,
                list_docker_images.offset,
                list_docker_images.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@docker_image_id_argument
@format_option
def docker_image(
    jobard_client: JobardClient,
    docker_image_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a Docker image.
    \f
    Args:
        jobard_client: jobard client
        docker_image_id: Docker image ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        image = jobard_client.docker_image(image_id=docker_image_id, token=token)
        image.accept(DockerVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.argument('name', type=str)
@click.argument('url', type=str)
@click.option(
    '--is_test',
    is_flag=True,
    help='The Docker is a test image',
)
@click.pass_obj
@token_option
def add_docker_image(
    jobard_client: JobardClient,
    name: str,
    url: str,
    token: str,
    is_test: bool,
):
    """Create a Docker image.
    \f
    Args:
        jobard_client: jobard client
        name: Docker image name
        url: Docker image url
        is_test: Boolean indicating whether the Docker image is a test image
        token: authentication token
    """
    try:
        response = jobard_client.add_docker_image(
            docker_image=DockerImageInput(
                name=name,
                url=url,
                is_test=is_test,
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@docker_image_id_argument
@click.argument('name', type=str)
@click.argument('url', type=str)
@click.option(
    '--is_test',
    is_flag=True,
    help='The Docker is a test image',
)
@click.pass_obj
@token_option
@format_option
def update_docker_image(
    jobard_client: JobardClient,
    docker_image_id: int,
    name: str,
    url: str,
    output_format: Format,
    token: str,
    is_test: bool,
):
    """Update a Docker image.
    \f
    Args:
        jobard_client: jobard client
        docker_image_id: Docker image id
        name: Docker image name
        url: Docker image url
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        is_test: Boolean indicating whether the Docker image is a test image
        token: authentication token
    """
    try:
        updated_docker_image = jobard_client.update_docker_image(
            docker_image=DockerImageUpdate(
                image_id=docker_image_id,
                name=name,
                url=url,
                is_test=is_test,
            ),
            token=token,
        )
        click.echo(updated_docker_image)
        updated_docker_image.accept(DockerVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@docker_image_id_argument
def delete_docker_image(
    jobard_client: JobardClient,
    docker_image_id: int,
    token: str,
):
    """Delete a Docker image.
    \f
    Args:
        jobard_client: jobard client
        docker_image_id: Docker image ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_docker_image(image_id=docker_image_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@offset_option
@limit_option
@docker_image_id_argument
@format_option
def docker_mount_points(
    jobard_client: JobardClient,
    docker_image_id: int,
    offset: int,
    limit: int,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the mount points of a Docker image.
    \f
    Args:
        jobard_client: jobard client
        docker_image_id: Docker image id
        offset: pagination offset
        limit: pagination limit
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_docker_mount_points = jobard_client.docker_mount_points(
            token=token,
            image_id=docker_image_id,
            offset=offset,
            limit=limit,
        )

        if not list_docker_mount_points.count:
            sys.exit(0)

        list_docker_mount_points.accept(DockerVisitorFactory.create_visitor(output_format))

        if list_docker_mount_points.count > (list_docker_mount_points.offset + list_docker_mount_points.limit):
            click.echo('*' * 70)
            click.echo('Nb docker mount points : {0} ; Offset : {1} ; Limit : {2}'.format(
                list_docker_mount_points.count,
                list_docker_mount_points.offset,
                list_docker_mount_points.limit,
            ))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@docker_mount_point_id_argument
@format_option
def docker_mount_point(
    jobard_client: JobardClient,
    docker_mount_point_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a Docker mount point.
    \f
    Args:
        jobard_client: jobard client
        docker_mount_point_id: Docker mount point ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        mount_point = jobard_client.docker_mount_point(mount_point_id=docker_mount_point_id, token=token)
        mount_point.accept(DockerVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.argument('image_id', type=int)
@click.argument('name', type=str)
@click.argument('source', type=str)
@click.argument('destination', type=str)
@click.pass_obj
@token_option
def add_docker_mount_point(
    jobard_client: JobardClient,
    image_id: int,
    name: str,
    source: str,
    destination: str,
    token: str,
):
    """Create a Docker mount point.
    \f
    Args:
        jobard_client: jobard client
        image_id: Docker image id
        name: Docker mount point name
        source: Docker mount point source
        destination: Docker mount point destination
        token: authentication token
    """
    try:
        docker_mount_point_id = jobard_client.add_docker_mount_point(
            docker_mount_point=DockerMountPointInput(
                image_id=image_id,
                name=name,
                source=source,
                destination=destination,
            ),
            token=token,
        )
        click.echo(docker_mount_point_id)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@docker_mount_point_id_argument
@click.argument('name', type=str)
@click.argument('source', type=str)
@click.argument('destination', type=str)
@click.pass_obj
@token_option
@format_option
def update_docker_mount_point(
    jobard_client: JobardClient,
    docker_mount_point_id: int,
    name: str,
    source: str,
    destination: str,
    output_format: Format,
    token: str,
):
    """Update a Docker mount point.
    \f
    Args:
        jobard_client: jobard client
        docker_mount_point_id: Docker mount point id
        name: Docker mount point name
        source: Docker mount point source
        destination: Docker mount point destination
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_docker_mount_point = jobard_client.update_docker_mount_point(
            docker_mount_point=DockerMountPointUpdate(
                mount_point_id=docker_mount_point_id,
                name=name,
                source=source,
                destination=destination,
            ),
            token=token,
        )
        click.echo(updated_docker_mount_point)
        updated_docker_mount_point.accept(DockerVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@docker_mount_point_id_argument
def delete_docker_mount_point(
    jobard_client: JobardClient,
    docker_mount_point_id: int,
    token: str,
):
    """Delete a Docker mount point.
    \f
    Args:
        jobard_client: jobard client
        docker_mount_point_id: Docker mount point ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_docker_mount_point(mount_point_id=docker_mount_point_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.pass_obj
@token_option
@click.option(
    '--connection',
    type=str,
    default='localhost',
    help='Execute command on a remote cluster via an ssh connection',
)
@click.option(
    '--job-extra',
    '-J',
    type=str,
    default=['image=gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest'],
    multiple=True,
    help='Extra job configuration (multiple values allowed)',
)
def test(
    jobard_client: JobardClient,
    connection: str,
    job_extra: List[str],
    token: str,
):
    """Process integration tests.

    Runs successively:
    - test directly Jobard API health
    - test Jobard client
    - test Jobard daemon by submitting an echo command
    \f
    Args:
        jobard_client: jobard client
        connection: host connection
        job_extra: extra job configuration
        token: authentication token
    """

    # Test API
    try:
        jobard_client.health()
        click.echo(click.style('API ............. OK'))
    except Exception:
        click.echo(click.style('API ............. ERROR', fg=COLOR_RED))

    # Test Jobard client : check invalid joborder id
    try:
        jobard_client.stat(job_order_id=0, token=token)
    except IOError:
        click.echo(click.style('CLIENT .......... ERROR', fg=COLOR_RED))
    except UnknownJobOrderError:
        click.echo(click.style('CLIENT .......... OK'))

    # Test Jobard daemon
    job_order_id = None
    try:
        # submit a job order and retrieve job order id
        job_order_id = jobard_client.submit(
            joborder=JobOrderSubmit(
                command=['echo'],
                arguments=[['Hello Jobard !']],
                connection=connection,
                job_extra=job_extra
            ),
            token=token,
        )
    except (JobardError, IOError):
        click.echo(click.style('DAEMON .......... ERROR', fg=COLOR_RED))
        return

    try:
        job_order_stat = jobard_client.stat(job_order_id=job_order_id, token=token)
    except (JobardError, IOError):
        click.echo(click.style('DAEMON .......... ERROR', fg=COLOR_RED))
        return

    click.echo(click.style('DAEMON '), nl=False)

    # Check the status of the job, if the job is not finished after max_checks checks, the daemon is considered in error
    nb_stat = 0
    max_checks = 10
    while job_order_id is not None and (job_order_stat.state not in states_closed and nb_stat < max_checks):
        time.sleep(10)
        try:
            job_order_stat = jobard_client.stat(job_order_id=job_order_id, token=token)
            click.echo(click.style('.'), nl=False)
            nb_stat = nb_stat + 1
        except (JobardError, IOError):
            click.echo(click.style(' ERROR', fg=COLOR_RED))
            return

    spaces = ' ' * (max_checks - nb_stat)
    if nb_stat == max_checks:
        click.echo(click.style('{0} ERROR'.format(spaces), fg=COLOR_RED))
    elif job_order_stat.state != State.SUCCEEDED:
        click.echo(click.style('{0} ERROR'.format(spaces), fg=COLOR_RED))
    else:
        dots = '.' * (max_checks - nb_stat)
        click.echo(click.style('{0} OK'.format(dots)))


@cli.command()
@click.pass_obj
def health(
    jobard_client: JobardClient
):
    """Check jobard health.
    Return monitoring results (jobard api, database and daemons health per host connection).
    \f
    Args:
        jobard_client: jobard client
    """
    try:
        monitoring_obj = jobard_client.health()
        monitoring_obj.accept(MonitoringVisitorFactory.create_visitor(Format.JSON))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@connection_name_argument
@cluster_id_argument
@connection_daemon_id_argument
@click.pass_obj
@token_option
def add_connection(
    jobard_client: JobardClient,
    connection_name: str,
    connection_daemon_id: str,
    cluster_id: int,
    token: str,
):
    """Create a remote connection.
    \f
    Args:
        jobard_client: jobard client
        connection_name: remote connection name
        connection_daemon_id: remote connection daemon ID
        cluster_id: cluster ID
        token: authentication token
    """
    try:
        response = jobard_client.add_connection(
            connection=RemoteConnectionInput(
                name=connection_name,
                daemon_id=connection_daemon_id,
                cluster_id=cluster_id
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@connection_name_argument
@cluster_id_argument
@connection_daemon_id_argument
@click.pass_obj
@token_option
@format_option
def update_connection(
    jobard_client: JobardClient,
    connection_name: str,
    connection_daemon_id: str,
    cluster_id: int,
    output_format: Format,
    token: str
):
    """Update a remote connection.
    \f
    Args:
        jobard_client: jobard client
        connection_name: remote connection name
        connection_daemon_id: remote connection daemon ID
        cluster_id: cluster ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_connection = jobard_client.update_connection(
            connection=RemoteConnectionUpdate(
                name=connection_name,
                daemon_id=connection_daemon_id,
                cluster_id=cluster_id
            ),
            token=token,
        )
        click.echo(updated_connection)
        updated_connection.accept(MonitoringVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@connection_name_argument
@connection_daemon_id_argument
@click.pass_obj
@token_option
def delete_connection(
    jobard_client: JobardClient,
    connection_name: str,
    connection_daemon_id: str,
    token: str,
):
    """Delete a remote connection.
    \f
    Args:
        jobard_client: jobard client
        connection_name: remote connection name
        connection_daemon_id: remote connection daemon ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_connection(name=connection_name, daemon_id=connection_daemon_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@format_option
def connections(
    jobard_client: JobardClient,
    output_format: Format
):  # noqa: WPS125
    """List the host connections of the jobard daemons.
    \f
    Args:
        jobard_client: jobard client
        output_format: output format [TABLE|CSV|JSON], default : TABLE
    """
    try:
        list_connections = jobard_client.connections()
        list_connections.accept(MonitoringVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.pass_obj
@token_option
@connection_name_argument
@connection_daemon_id_argument
@format_option
def connection(
    jobard_client: JobardClient,
    connection_name: str,
    connection_daemon_id: str,
    output_format: Format,
    token: str,
):
    """Retrieve a remote connection.
    \f
    Args:
        jobard_client: jobard client
        connection_name: connection name
        connection_daemon_id: daemon ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        conn = jobard_client.connection(name=connection_name, daemon_id=connection_daemon_id, token=token)
        conn.accept(MonitoringVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@click.argument('cluster_name', type=str)
@cluster_type_option
@healthcheck_interval_option
@remote_username_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
def add_cluster(
    jobard_client: JobardClient,
    cluster_name: str,
    cluster_type: Cluster,
    healthcheck_interval: timedelta,
    remote_username: str,
    password: str,
    ssh_key_file: str,
    token: str
):
    """Create a Jobard cluster.
    \f
    Args:
        jobard_client: jobard client
        cluster_name: Jobard cluster name
        cluster_type: Jobard cluster type
        healthcheck_interval: healthcheck interval
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        token: authentication token
    """

    try:
        response = jobard_client.add_cluster(
            cluster=JobardClusterInput(
                cluster_name=cluster_name,
                type=cluster_type,
                healthcheck_interval=healthcheck_interval,
                remote_username=remote_username,
                password=password,
                client_key=ssh_key_file
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@cluster_id_argument
@click.argument('cluster_name', type=str)
@cluster_type_option
@healthcheck_interval_option
@remote_username_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
@format_option
def update_cluster(
    jobard_client: JobardClient,
    cluster_id: int,
    cluster_name: str,
    cluster_type: Cluster,
    healthcheck_interval: timedelta,
    remote_username: str,
    password: str,
    ssh_key_file: Path,
    output_format: Format,
    token: str
):
    """Update a Jobard cluster.
    \f
    Args:
        jobard_client: jobard client
        cluster_id: Jobard cluster ID
        cluster_name: Jobard cluster name
        cluster_type: Jobard cluster type
        healthcheck_interval: healthcheck interval
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_cluster = jobard_client.update_cluster(
            cluster=JobardClusterUpdate(
                cluster_id=cluster_id,
                cluster_name=cluster_name,
                type=cluster_type,
                healthcheck_interval=healthcheck_interval,
                remote_username=remote_username,
                password=password,
                client_key=ssh_key_file
            ),
            token=token,
        )
        click.echo(updated_cluster)
        updated_cluster.accept(MonitoringVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@cluster_id_argument
@click.pass_obj
@token_option
def delete_cluster(
    jobard_client: JobardClient,
    cluster_id: int,
    token: str,
):
    """Delete a Jobard cluster.
    \f
    Args:
        jobard_client: jobard client
        cluster_id: Jobard cluster ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_cluster(cluster_id=cluster_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.pass_obj
@token_option
@cluster_id_argument
@format_option
def cluster(
    jobard_client: JobardClient,
    cluster_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a Jobard cluster.
    \f
    Args:
        jobard_client: jobard client
        cluster_id: Jobard cluster ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        image = jobard_client.cluster(cluster_id=cluster_id, token=token)
        image.accept(MonitoringVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@format_option
def clusters(
    jobard_client: JobardClient,
    output_format: Format,
):  # noqa: WPS125
    """List the clusters stored in the database.
    \f
    Args:
        jobard_client: jobard client
        output_format: output format [TABLE|CSV|JSON], default : TABLE
    """
    try:
        list_clusters = jobard_client.clusters()
        list_clusters.accept(MonitoringVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.argument('user_name', type=str)
@click.argument('user_email', type=str)
@click.argument('remote_username', type=str)
@unix_uid_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
def add_user(
    jobard_client: JobardClient,
    user_name: str,
    user_email: str,
    unix_uid: int,
    remote_username: str,
    password: str,
    ssh_key_file: str,
    token: str
):
    """Create a Jobard user.
    \f
    Args:
        jobard_client: jobard client
        user_name: Jobard user name
        user_email: Jobard user email address
        unix_uid: jobard user Unix UID
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        token: authentication token
    """

    try:
        response = jobard_client.add_user(
            user=JobardUserInput(
                username=user_name,
                email=user_email,
                unix_uid=unix_uid,
                remote_username=remote_username,
                password=password,
                client_key=ssh_key_file
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@user_id_argument
@click.argument('user_name', type=str)
@click.argument('user_email', type=str)
@click.argument('remote_username', type=str)
@unix_uid_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
@format_option
def update_user(
    jobard_client: JobardClient,
    user_id: int,
    user_name: str,
    user_email: str,
    unix_uid: int,
    remote_username: str,
    password: str,
    ssh_key_file: str,
    output_format: Format,
    token: str
):
    """Update a Jobard user.
    \f
    Args:
        jobard_client: jobard client
        user_id: Jobard user ID
        user_name: Jobard user name
        user_email: Jobard user email address
        unix_uid: jobard user Unix UID
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_user = jobard_client.update_user(
            user=JobardUserUpdate(
                user_id=user_id,
                username=user_name,
                email=user_email,
                unix_uid=unix_uid,
                remote_username=remote_username,
                password=password,
                client_key=ssh_key_file
            ),
            token=token,
        )
        click.echo(updated_user)
        updated_user.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@user_id_argument
@click.pass_obj
@token_option
def delete_user(
    jobard_client: JobardClient,
    user_id: int,
    token: str,
):
    """Delete a Jobard user.
    \f
    Args:
        jobard_client: jobard client
        user_id: Jobard user ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_user(user_id=user_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.pass_obj
@token_option
@user_id_argument
@format_option
def user(
    jobard_client: JobardClient,
    user_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a Jobard user.
    \f
    Args:
        jobard_client: jobard client
        user_id: Jobard user ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        image = jobard_client.user(user_id=user_id, token=token)
        image.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@format_option
def users(
    jobard_client: JobardClient,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the users stored in the database.
    \f
    Args:
        jobard_client: jobard client
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_users = jobard_client.users(token=token)
        list_users.accept(UserVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@cluster_access_name_argument
@user_id_argument
@cluster_id_argument
@click.argument('remote_app_path', type=str)
@click.option(
    '--log_root',
    type=str,
    help='The Jobard logs root path',
)
@click.option(
    '--entry_point',
    type=str,
    default='jobard-remote-dask',
    help='The entry point (script), into the Jobard remote application directory. Default : jobard-remote-dask',
)
@click.option(
    '--entry_point_wrapper',
    type=str,
    help='The Jobard entry point wrapper',
)
@click.pass_obj
@token_option
def add_cluster_access(
    jobard_client: JobardClient,
    cluster_access_name: str,
    user_id: int,
    cluster_id: int,
    remote_app_path: str,
    log_root: str,
    entry_point: str,
    entry_point_wrapper: str,
    token: str,
):
    """Create a cluster access.
    \f
    Args:
        jobard_client: jobard client
        cluster_access_name: cluster access name
        user_id: Jobard user ID
        cluster_id: cluster ID
        remote_app_path: Jobard remote application directory
        log_root: Jobard logs root path
        entry_point: the entry point (script), into the Jobard remote application directory
        entry_point_wrapper: python path (/bin/python). it could be a python binary into a conda env.
        token: authentication token
    """
    try:
        response = jobard_client.add_cluster_access(
            cluster_access=ClusterAccessInput(
                cluster_access_name=cluster_access_name,
                user_id=user_id,
                cluster_id=cluster_id,
                remote_app_path=remote_app_path,
                log_root=log_root,
                entry_point=entry_point,
                entry_point_wrapper=entry_point_wrapper
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@cluster_access_id_argument
@cluster_access_name_argument
@user_id_argument
@cluster_id_argument
@click.argument('remote_app_path', type=str)
@click.option(
    '--log_root',
    type=str,
    help='The Jobard logs root path',
)
@click.option(
    '--entry_point',
    type=str,
    default='jobard-remote-dask',
    help='The entry point (script), into the Jobard remote application directory. Default : jobard-remote-dask',
)
@click.option(
    '--entry_point_wrapper',
    type=str,
    help='The Jobard entry point wrapper',
)
@click.pass_obj
@token_option
@format_option
def update_cluster_access(
    jobard_client: JobardClient,
    cluster_access_id: int,
    cluster_access_name: str,
    user_id: int,
    cluster_id: int,
    remote_app_path: str,
    log_root: str,
    entry_point: str,
    entry_point_wrapper: str,
    output_format: Format,
    token: str
):
    """Update a cluster access.
    \f
    Args:
        jobard_client: jobard client
        cluster_access_id: cluster access ID
        cluster_access_name: cluster access name
        user_id: Jobard user ID
        cluster_id: cluster ID
        remote_app_path: Jobard remote application directory
        log_root: Jobard logs root path
        entry_point: the entry point (script), into the Jobard remote application directory
        entry_point_wrapper: python path (/bin/python). it could be a python binary into a conda env.
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_cluster_access = jobard_client.update_cluster_access(
            cluster_access=ClusterAccessUpdate(
                cluster_access_id=cluster_access_id,
                cluster_access_name=cluster_access_name,
                user_id=user_id,
                cluster_id=cluster_id,
                log_root=log_root,
                remote_app_path=remote_app_path,
                entry_point_wrapper=entry_point_wrapper,
                entry_point=entry_point
            ),
            token=token,
        )
        click.echo(updated_cluster_access)
        updated_cluster_access.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@cluster_access_id_argument
@click.pass_obj
@token_option
def delete_cluster_access(
    jobard_client: JobardClient,
    cluster_access_id: int,
    token: str,
):
    """Delete a cluster access.
    \f
    Args:
        jobard_client: jobard client
        cluster_access_id: cluster access ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_cluster_access(cluster_access_id=cluster_access_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@format_option
def cluster_accesses(
    jobard_client: JobardClient,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the cluster_accesses.
    \f
    Args:
        jobard_client: jobard client
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_cluster_accesses = jobard_client.cluster_accesses(token=token)
        list_cluster_accesses.accept(UserVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.pass_obj
@token_option
@cluster_access_id_argument
@format_option
def cluster_access(
    jobard_client: JobardClient,
    cluster_access_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve a cluster access.
    \f
    Args:
        jobard_client: jobard client
        cluster_access_id: cluster access ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        conn = jobard_client.cluster_access(cluster_access_id=cluster_access_id, token=token)
        conn.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@user_id_argument
@click.argument('access_token', type=str)
@click.pass_obj
@token_option
def add_access_token(
    jobard_client: JobardClient,
    user_id: int,
    access_token: str,
    token: str
):
    """Create an access token for a Jobard user.
    \f
    Args:
        jobard_client: jobard client
        user_id: Jobard user ID
        access_token: access token value
        token: authentication token
    """

    try:
        response = jobard_client.add_access_token(
            access_token=AccessTokenInput(
                user_id=user_id,
                access_token=access_token
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@access_token_id_argument
@user_id_argument
@click.argument('access_token', type=str)
@click.pass_obj
@token_option
@format_option
def update_access_token(
    jobard_client: JobardClient,
    access_token_id: int,
    user_id: int,
    access_token: str,
    output_format: Format,
    token: str
):
    """Update an access token.
    \f
    Args:
        jobard_client: jobard client
        access_token_id: access_token ID
        access_token: access token value
        user_id: Jobard user ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        updated_access_token = jobard_client.update_access_token(
            access_token=AccessTokenUpdate(
                token_id=access_token_id,
                access_token=access_token,
                user_id=user_id
            ),
            token=token,
        )
        click.echo(updated_access_token)
        updated_access_token.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))


@cli.command()
@access_token_id_argument
@click.pass_obj
@token_option
def delete_access_token(
    jobard_client: JobardClient,
    access_token_id: int,
    token: str,
):
    """Delete an access token.
    \f
    Args:
        jobard_client: jobard client
        access_token_id: access token ID
        token: authentication token
    """
    try:
        response = jobard_client.delete_access_token(access_token_id=access_token_id, token=token)
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.pass_obj
@token_option
@access_token_id_argument
@format_option
def access_token(
    jobard_client: JobardClient,
    access_token_id: int,
    output_format: Format,
    token: str,
):
    """Retrieve an access token.
    \f
    Args:
        jobard_client: jobard client
        access_token_id: access token ID
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        image = jobard_client.access_token(access_token_id=access_token_id, token=token)
        image.accept(UserVisitorFactory.create_visitor(output_format))
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()  # noqa: WPS216, WPS125, WPS211
@click.pass_obj
@token_option
@format_option
def access_tokens(
    jobard_client: JobardClient,
    output_format: Format,
    token: str,
):  # noqa: WPS125
    """List the access tokens stored in the database.
    \f
    Args:
        jobard_client: jobard client
        output_format: output format [TABLE|CSV|JSON], default : TABLE
        token: authentication token
    """
    try:
        list_access_tokens = jobard_client.access_tokens(token=token)
        list_access_tokens.accept(UserVisitorFactory.create_visitor(output_format))

    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@connection_name_argument
@click.argument('cluster_name', type=str)
@connection_daemon_id_argument
@cluster_type_option
@healthcheck_interval_option
@remote_username_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
def add_connection_cluster(
    jobard_client: JobardClient,
    connection_name: str,
    connection_daemon_id: str,
    cluster_name: str,
    cluster_type: Cluster,
    healthcheck_interval: timedelta,
    remote_username: str,
    password: str,
    ssh_key_file: str,
    token: str,
):
    """Create a remote connection and its cluster.
    \f
    Args:
        jobard_client: jobard client
        connection_name: remote connection name
        connection_daemon_id: remote connection daemon ID
        cluster_name: Jobard cluster name
        cluster_type: Jobard cluster type
        healthcheck_interval: healthcheck interval
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        token: authentication token
    """
    try:
        response = jobard_client.add_connection_cluster(
            connection_cluster=RemoteConnectionClusterInput(
                name=connection_name,
                daemon_id=connection_daemon_id,
                cluster=JobardClusterInput(
                    cluster_name=cluster_name,
                    type=cluster_type,
                    healthcheck_interval=healthcheck_interval,
                    remote_username=remote_username,
                    password=password,
                    client_key=ssh_key_file
                )
            ),
            token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

@cli.command()
@click.argument('user_name', type=str)
@click.argument('user_email', type=str)
@click.argument('remote_username', type=str)
@click.argument('access_token', type=str)
@unix_uid_option
@password_option
@ssh_key_file_option
@click.pass_obj
@token_option
def add_user_access(
    jobard_client: JobardClient,
    user_name: str,
    user_email: str,
    unix_uid: int,
    remote_username: str,
    password: str,
    ssh_key_file: str,
    access_token: str,
    token: str,
):
    """Create a Jobard user and its token.
    \f
    Args:
        jobard_client: jobard client
        user_name: Jobard user name
        user_email: Jobard user email address
        unix_uid: jobard user Unix UID
        remote_username: jobard user remote name
        password: jobard user password
        ssh_key_file: jobard user SSH key file path
        access_token: jobard user access token value
        token: authentication token
    """
    try:
        response = jobard_client.add_user_access(
            user_access=UserAccessInput(
                user=JobardUserInput(
                    username=user_name,
                    email=user_email,
                    unix_uid=unix_uid,
                    remote_username=remote_username,
                    password=password,
                    client_key=ssh_key_file
                ),
                access_token=access_token,

                ),
                token=token,
        )
        click.echo(response)
    except JobardError as error:
        click.echo(click.style(error, fg=COLOR_RED))

def main():
    """Run jobard client console script."""
    cli()


if __name__ == '__main__':
    main()
