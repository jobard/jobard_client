# Jobard Client

Python client for Job Array Daemon.

Documentation : https://jobard.gitlab-pages.ifremer.fr/documentation

## Development

### Install dependencies

#### Basic installation

Recommended if you just need to access to Jobard-client from python script.

```bash
poetry install -vv
```
 #### Installation with commands 

This installation with extra allows to execute Jobard-client commands from a terminal.

```bash
poetry install -vv --extras renderers
```

### Pre-commit

- register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
pytest --tb=line
```
